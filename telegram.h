#ifndef TELEGRAM_H
#define TELEGRAM_H

#include "debug.h"
#include "network.h"
#include "includes.h"
#include "message.h"
#include "user.h"
#include "social.h"

// overloaded {{{
namespace detail {
    template <class... Fs>
    struct overload;

    template <class F>
    struct overload<F> : public F {
        explicit overload(F f) : F(f) { }
    };

    template <class F, class... Fs>
    struct overload<F, Fs...> : public overload<F>, overload<Fs...> {
        overload(F f, Fs... fs)
            : overload<F>(f),
              overload<Fs...>(fs...)
        { }

        using overload<F>::operator();
        using overload<Fs...>::operator();
    };
}

template <class... F> auto overloaded(F... f) {
    return detail::overload<F...>(f...);
}
// }}}

using namespace td;
using Object = td_api::object_ptr<td_api::Object>;
typedef uint64_t ull;

class Telegram : public QObject, public SocialInterface
{
    Q_OBJECT
public:
    explicit Telegram(QObject *parent = 0);
    ~Telegram();
    static Telegram* foo;
    static const qs name;

    void loadHistory  (qs peerId, int count, long long startMessageId = -1, bool force = false);
    void loadUser     (qs peerId);
    void loadUsers    (const std::vector<qs>& peerIds);
    void markAsRead   (qs peerId, const vector<ll>& messageIds);
    void sendMessage  (qs peerId, qs const& text, map<string, QVariant> options);
    void deleteMessage(message const& m);
    void editMessage  (message const& m);
    void setDraft     (message const& m);
    void hideUser     (qs peerId, bool hidden);
    void erect        (qs peerId, ll messageId);

    void init(const QJsonValue& config);
public slots:
    void startLongPoll();
signals:
    void onUserLoaded(user const& u);
    void onUserUpdated(qs peerId, long long inRead, long long outRead, int unread);
    void onMessageRecieved(message const& m);
    void onMessageEdited(message const& m);
    void log(const QVariantList& list);
    void onUnreadDialogs(int dialogs);
    void userIsTyping(qs peerId, qs userId);
    void onDraftMessage(const message& m);
    void onUserHidden(qs peerId, bool hidden);

private:
    static void send_query(td_api::object_ptr<td_api::Function> f, function<void(Object)> handler);
    static void process_response(Client::Response response);
    static void on_authorization_stateupdate();
    static void process_update(Object update);
    static function<void(Object)> create_authentication_query_handler();
    static void editOrSendMessage(qs peerId, qs text, const map<string, QVariant>& options = {});
    static message getMessageFromApi(const td_api::message& m);
    static void getFile(message& ret, const td_api::file& p, const int& height, const string& filename = "");

    static void longPoll();
    static void proceedLastDialogs();
    template<typename... Tail>
    static void writeln(const Tail&... tail);
    static void print(const QVariantList& list);
    template<typename T = int>
    static function<void(Object)> errorHandler(const qs& title, const function<void(td_api::object_ptr<T>)>& f = [](td_api::object_ptr<int>){});
    static void setMessageText(message& ret, const td_api::MessageContent&& content);
    static qs getFromId(const td_api::MessageSender& sender);
    static qs splitPeer(qs& peerId);
        
    //variables
        static int lastTS;
        static int lastMessageId;
        static volatile bool isRunning;
        static QFutureWatcher<void>* watcher;

        static unique_ptr<td::Client> client;

        static bool are_authorized;
        static bool need_restart;
        static ull current_query_id;
        static ull authentication_query_id;

        static ll api_id;
        static qs api_hash;

        static td_api::object_ptr<td_api::AuthorizationState> authorization_state;
        static map<ull, function<void(Object)>> handlers;
        static map<ll, tuple<message, int, string>> files;
        static map<ll, bool> forums_loaded;
    };

#endif // TELEGRAM_H
