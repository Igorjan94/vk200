#ifndef NETWORK_H
#define NETWORK_H

#include "includes.h"
#include "debug.h"

class network : public QObject
{
    Q_OBJECT
public:
    static QJsonObject send(const qs& http, const map<qs, qs>& queryParams, optional<pair<qs, qs>> photo = {});
    static const qs name;
signals:
    void docIsLoaded();
    void log(const QVariantList& list);
public:
    static network* foo;
private:
    template<typename... Tail>
    static void writeln(const Tail&... tail);
};

#endif //NETWORK_H
