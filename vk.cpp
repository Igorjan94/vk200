#include "vk.h"
#undef emit
#define emit foo->

//SLOTS {{{
void Vk::loadHistory(qs peerId, int count, ll startMessageId, [[maybe_unused]] bool force) //If count < 0 then get messageId == -count only {{{
{
    map<qs, qs> params;
    params["peer_id"] = peerId;
    params["extended"] = "1";
    if (count < 0)
        params["count"] = "1",
        params["start_message_id"] = qs::number(-count);
    else
        params["count"] = qs::number(count);
    if (startMessageId != -1)
        params["start_message_id"] = qs::number(startMessageId),
        params["offset"] = "1";
    auto res = request("messages.getHistory", params);
    if (res.has_value())
    {
        const auto& response = getValue(res.value(), "response");
        const auto& profiles = getArray(response, "profiles");
        const auto& groups = getArray(response, "groups");
        const auto& conversations = getArray(response, "conversations");
        const auto& messages = getArray(response, "items");

        proceedProfiles(profiles);
        proceedGroups(groups);
        for (const auto& conversation: conversations)
            proceedConversation(conversation);
        for (int i = size(messages) - 1; i >= 0; --i)
            emit onMessageRecieved(getMessageFromJson(messages[i]));
    }
    else
        writeln("Не удалось получить историю сообщений пользователя", peerId);
} //}}}

void Vk::loadUser(qs peerId) //{{{
{
    loadUsers({peerId});
}
//}}}

void Vk::loadUsers(const vector<qs>& peerIds) //{{{
{
    QStringList groups, chats, users;
    for (auto& userId : peerIds)
    {
        writeln("Пользователь", userId, "не существует, получаем из ВК");
        if (userId[0] == '-') //Группа
            groups.push_back(qs(userId).remove(0, 1));
        else if (userId.toInt() < CHAT_OFFSET) //Пользователь
            users.push_back(userId);
        else //Чат
            chats.push_back(qs::number(userId.toInt() - CHAT_OFFSET));
    }
    if (!users.isEmpty())
        for (auto&& userDetails: getArrayOptional(request("users.get", {{"user_ids", users.join(",")}}), "response"))
            emit onUserLoaded(user(getInt(userDetails, "id"), getString(userDetails, "first_name") + " " + getString(userDetails, "last_name")));
    if (!groups.isEmpty())
        for (auto&& userDetails: getArrayOptional(request("groups.getById", {{"group_ids", groups.join(",")}}), "response"))
            emit onUserLoaded(user(-getInt(userDetails, "id"), "Сообщество: " + getString(userDetails, "name")));
    if (!chats.isEmpty())
        for (auto&& userDetails: getArrayOptional(request("messages.getChat", {{"chat_ids", chats.join(",")}}), "response"))
            emit onUserLoaded(user(getInt(userDetails, "id") + CHAT_OFFSET, "Чат: " + getString(userDetails, "title")));
} //}}}

void Vk::markAsRead(qs peerId, const vector<ll>& messageIds) // {{{
{
    auto x = request("messages.markAsRead", {{"peer_id", peerId}, {"start_message_id", qs::number(*max_element(messageIds.begin(), messageIds.end()))}});
    if (x.has_value())
        writeln("Пользователь", peerId, "помечен прочитанным", getInt(x.value(), "response"));
    else
        writeln("Не удалось пометить пользователя", peerId, "прочитанным");
} //}}}

void Vk::sendMessage(qs peerId, const qs& text, map<string, QVariant> options) // {{{
{
    if (text.size() == 0) return;
    if (!text.startsWith("/"))
        editOrSendMessage(peerId, text, options);
    else
    {
        auto raw = qs(text).remove(0, 1);
        auto spl = qs(raw).replace("\n", " ").split(" ");
        auto command = spl[0];
        if (command == "execute" && spl.size() >= 2)
        {
            writeln("Выполняю команду:", "/" + text);
            spl.removeAt(0);
            sendCommand(spl, [&](QJsonObject x) {
                writeln(toString(x));
            });
        }
        else if (command == "friends")
        {
            writeln("Запрашиваю запросы на добавление в друзья");
            sendCommand({"friends.getRequests"}, [&](QJsonObject x) {
                if (getInt(x, "response.count") == 0)
                    writeln("Нет запросов на добавление в друзья");
                const auto& friends = getArray(x, "response.items");
                for (const auto& id : friends)
                    writeln("https://vk.com/id" + qs::number(id.toInt()), id.toInt());
            });
        }
        else if (command == "addFriendRequest" && spl.size() >= 2)
        {
            qs userId = spl[1];
            writeln("Подтверждаю запрос на добавление в друзья", userId);
            sendCommand({"friends.add", "userId", userId}, [&](QJsonObject x) {
                writeln("Запрос пользователя", userId, "подтвержден", toString(x));
            });
        }
        else if (command == "deleteFriendRequest" && spl.size() >= 2)
        {
            qs userId = spl[1];
            writeln("Удаляю запрос на добавление в друзья", userId);
            sendCommand({"friends.delete", "userId", userId}, [&](QJsonObject x) {
                writeln("Запрос пользователя", userId, "удален", toString(x));
            });
        }
        else if ((command == "attach" || command == "a") && spl.size() >= 2)
        {
            options["attachment"] = spl[1];
            editOrSendMessage(peerId, "", options);
        }
        else if ((command == "image" || command == "i" || command == "doc") && spl.size() >= 2)
        {
            qs type = command[0] == 'i' ? "photo" : "doc";
            qs type2 = command[0] == 'i' ? "photo" : "file";
            sendCommand({type + "s.getMessagesUploadServer", "peer_id", peerId}, [&](QJsonObject x) {
                qs url = getString(x, "response.upload_url");
                auto capSpl = raw.split("//");
                spl = capSpl[0].split(" ");
                spl.removeAt(0);
                capSpl.removeAt(0);
                auto caption = capSpl.join("//");

                QStringList photos;

                for (auto& ppp : spl)
                if (ppp.size())
                {
                    auto photo = network::send(url, {}, pair(type2, ppp.replace("%20", " ")));
                    map<qs, qs> body;
                    qs method;
                    if (type == "doc")
                        body = {{"file", getString(photo, "file")}},
                        method = "docs.save";
                    else
                        body = {{"photo", getString(photo, "photo")}, {"server", qs::number(getInt(photo, "server"))}, {"hash", getString(photo, "hash")}},
                        method = "photos.saveMessagesPhoto";
                    auto savedPhoto = request(method, body);
                    if (savedPhoto.has_value())
                    {
                        QJsonValue y;
                        if (type == "doc") y = getValue(savedPhoto.value(), "response.doc"); else y = getArray(savedPhoto.value(), "response")[0];
                        auto ownerId = getInt(y, "owner_id");
                        auto id = getInt(y, "id");
                        photos.append(type + qs::number(ownerId) + "_" + qs::number(id));
                    }
                }

                if (!photos.isEmpty())
                {
                    options["attachment"] = photos.join(",");
                    editOrSendMessage(peerId, caption, options);
                }
            });
        }
        else
            editOrSendMessage(peerId, text, options);
    }
} //}}}

void Vk::deleteMessage(const message& m) //{{{
{
    auto x = request("messages.delete", {{"message_ids", qs::number(m.id)}, {"delete_for_all", "1"}});
    if (!x.has_value())
        writeln("Не удалось удалить сообщение", m.text);
    else
        writeln("Сообщение успешно удалено");
} // }}}

void Vk::editMessage(const message& m) //{{{
{
    editOrSendMessage(m.peerId, m.text, {{"messageId", m.id}});
} // }}}

void Vk::setDraft(const message& m) //{{{
{
    writeln("Setting draft message", m.peerId, m.text);
} // }}}

void Vk::hideUser(qs peerId, bool hidden) // {{{
{
    writeln("Hiding", peerId, hidden);
} // }}}

void Vk::print(const QVariantList& list) // {{{
{
    return;
    for (const auto& x: list)
        cout << x.toString() << " ";
    cout << endl;
} //}}}

template<typename... Tail> void Vk::writeln(const Tail&... tail) // {{{
{
    print(qv(tail...));
    emit log(qv(tail...));
} // }}}
//}}}

void Vk::editOrSendMessage(qs peerId, const qs& messageText, const map<string, QVariant>& options) // {{{
{
    qs attachment = "";
    if (options.find("attachment") != options.end())
        attachment = options.at("attachment").toString();
    qs method;
    qs text = qs(messageText).replace(TAB, "").replace("+", "%2B").replace("\\", "%5C");
    if (peerId == "245432263" && false)
        for (auto& [c, d]: vector<pair<qs, qs>>{{"о", "0"}, {"и", "1"}, {"д", "2"}, {"з", "3"}, {"ч", "4"}, {"п", "5"}, {"ш", "6"}, {"м", "7"}, {"в", "8"}, {"О", "0"}, {"И", "1"}, {"Д", "2"}, {"З", "3"}, {"Ч", "4"}, {"П", "5"}, {"Ш", "6"}, {"М", "7"}, {"В", "8"}})
            text.replace(c, d);
    map<qs, qs> params = {
        {"peer_id", peerId},
        {"message", text}
    };
    
    if (attachment != "") params["attachment"] = attachment;

    if (options.find("messageId") != options.end())
        method = "messages.edit",
        params["message_id"] = qs::number(options.at("messageId").toLongLong());
    else
        method = "messages.send",
        params["random_id"] = qs::number(QDateTime::currentMSecsSinceEpoch() / 1000);
    if (options.find("replyTo") != options.end())
        params["reply_to"] = qs::number(options.at("replyTo").toLongLong());

    auto x = request(method, params);
    if (x.has_value() && getInt(x.value(), "response"))
        writeln("Сообщение \"", text, attachment, "\" успешно отправлено", peerId, getInt(x.value(), "response"));
    else
        writeln("-------- Не удалось отправить сообщение \"" + text + "\" пользователю " + peerId + " --------");
} //}}}

void Vk::proceedGroups(const QJsonArray& groups) // {{{
{
    for (const auto& group: groups)
        emit onUserLoaded(user(
            -getInt(group, "id"),
            "Сообщество: " + getString(group, "name")
        ));
} // }}}

void Vk::proceedProfiles(const QJsonArray& profiles) // {{{
{
    for (const auto& profile: profiles)
        emit onUserLoaded(user(
            getInt(profile, "id"),
            getString(profile, "first_name") + " " + getString(profile, "last_name")
        ));
} // }}}

bool Vk::proceedConversation(const QJsonValue& conversation) // {{{
{
    qs type = getString(conversation, "peer.type");
    qs peerId = qs::number(getInt(conversation, "peer.id"));
    ll inRead = getInt(conversation, "in_read");
    ll outRead = getInt(conversation, "out_read");
    int unread = getInt(conversation, "unread_count");
    if (type == "chat") emit onUserLoaded(user(peerId, "Чат: " + getString(conversation, "chat_settings.title")));
    emit onUserUpdated(peerId, inRead, outRead, unread);
    return unread;
} //}}}

void Vk::proceedLastDialogs() // {{{
{
    writeln("Proceeding last dialogs...");
    auto y = request("messages.getConversations", {{"extended", "1"}});
    if (!y.has_value()) return;
    auto res = getValue(y.value(), "response");
    proceedProfiles(getArray(res, "profiles"));
    proceedGroups(getArray(res, "groups"));

    int unread = 0;
    const auto& dialogs = getArray(res, "items");
    for (const auto& dialog: dialogs)
        unread += proceedConversation(getValue(dialog, "conversation"));
    emit onUnreadDialogs(unread);
    writeln("Proceeded last dialogs");
} //}}}

void Vk::erect([[maybe_unused]] qs peerId, [[maybe_unused]] ll messageId) //{{{
{
} // }}}
// }}}

void Vk::proceedMessageAttachment(message& ret, const QJsonValue& attachment) // {{{
{
    auto loadPhoto = [&ret](const QJsonArray& photos, bool mx = true) {
        qs link;
        int w = mx ? 0 : 100000;
        int h = mx ? 0 : 100000;
        for (auto&& photo: photos)
        {
            auto ww = getInt(photo, "width");
            auto hh = getInt(photo, "height");
            if ((mx && ww + hh > w + h) || (!mx && ww + hh < w + h))
                w = ww, h = hh,
                link = getString(photo, "url");
        }
        ret.photos.emplace_back(link, w, h);
    };
    auto append = [&ret](const qs& s) {
        ret.text = join(ret.text, s);
    };

    qs type = getString(attachment, "type");

    if (type == "photo")
        return loadPhoto(getArray(attachment, "photo.sizes"));

    if (type == "sticker")
        return loadPhoto(getArray(attachment, "sticker.images"), false);

    if (type == "video")
    {
        auto video = getValue(attachment, "video");
        qs photo;
        qs ownerId = qs::number(getInt(video, "owner_id"));
        auto x = request("video.get", {{"owner_id", ownerId}, {"videos", qs("%1_%2_%3").arg(ownerId, qs::number(getInt(video, "id")), getString(video, "access_key"))}});
        if (!x.has_value())
            loadPhoto(getArray(video, "image"));
        else
            photo = qs(R"(<iframe width="800" height="450" src="%1"></iframe>)").arg(getString(getArray(x.value(), "response.items")[0], "player"));
        return append(join(
            qs("Видео \"%1\" (%2)").arg(
                getString(video, "title"),
                getDuration(getInt(video, "duration"))
            ),
            photo,
            getString(video, "description")
        ));
    } 

    if (type == "link")
    {
        auto link = getValue(attachment, "link");
        if (auto p = getValue(link, "photo"); !p.isNull() && !p.isUndefined()) 
            loadPhoto(getArray(link, "photo.sizes"));
        ret.links.emplace_back(getString(link, "url"), getString(link, "title"));
        return append(getString(link, "description"));
    }

    if (type == "doc")
    {
        auto doc = getValue(attachment, "doc");
        ret.links.emplace_back(getString(doc, "url"), getString(doc, "title"));
        return;
    }

    if (type == "gift")
    {
        auto gift = getValue(attachment, "gift");
        ret.links.emplace_back(getString(gift, "thumb_256"), "Подарок");
        return;
    }

    if (type == "audio")
    {
        auto audio = getValue(attachment, "audio");
        return append(qs("Аудио: %1, %2 (%3)").arg(getString(audio, "artist"), getString(audio, "title"), getDuration(getInt(audio, "duration"))));
    }

    if (type == "audio_message")
    {
        auto audio = getValue(attachment, "audio_message");
        ret.links.emplace_back(getString(audio, "link_mp3"), qs("Голосовое сообщение (%1)").arg(getDuration(getInt(audio, "duration"))));
        return append(getString(audio, "transcript"));
    }

    if (type == "market")
    {
        writeln(toString(attachment.toObject()));
        return append("<market>");
    }
    
    if (type == "market_album")
    {
        writeln(toString(attachment.toObject()));
        return append("<market_album>");
    }

    if (type == "wall_reply")
    {
        writeln(toString(attachment.toObject()));
        return append("<wall_reply>");
    }
    
    if (type == "story")
    {
        writeln(toString(attachment.toObject()));
        return append("<story>");
    }
    
    if (type == "poll")
    {
        auto poll = getValue(attachment, "poll");
        const auto& answers = getArray(poll, "answers");
        set<ll> ids;
        const auto& answerIds = getArray(poll, "answer_ids");
        for (const auto& x: answerIds) ids.insert(x.toInt());

        vector<QStringList> table;
        for (const auto& answer: answers)
            table.push_back({
                getString(answer, "text"),
                qs::number(getInt(answer, "votes")),
                qs::number(getValue(answer, "rate").toDouble()) + "%",
                ids.find(getInt(answer, "id")) == ids.end() ? "" : "✓"
            });

        return append(tableView("Опрос: " + getString(poll, "question"), "<pre style=\"margin-left: 32px; display: inline\">%1  %2  (%3) %4</pre>", table));
    }
    auto wall = attachment;
    if (type == "wall")
        wall = getValue(attachment, "wall");

    auto text = getString(wall, "text");
    auto date = getInt(wall, "date");
    auto authorId = getInt(wall, "owner_id") | getInt(wall, "to_id");
    //emit loadUser(qs::number(authorId));
    message repost(qs::number(authorId), text, 0, date);
    repost.links.emplace_back(
        qs("https://vk.com/wall%1_%2?access_key=%3").arg(
            qs::number(authorId),
            qs::number(getInt(wall, "id")),
            getString(wall, "access_key")
        ), 
        "Репост"
    );
    proceedMessageAttachments(repost, getArray(wall, "attachments"));
    proceedMessageAttachments(repost, getArray(wall, "copy_history"));
    ret.fwdMessages.push_back(repost);
} //}}}

void Vk::proceedMessageAttachments(message& ret, const QJsonArray& attachments) // {{{
{
    for (const auto& attachment: attachments)
        proceedMessageAttachment(ret, attachment);
} //}}}

message Vk::getMessageFromJson(const QJsonValue& mes) // {{{
{
    ll id = getInt(mes, "id");

    int date = getInt(mes, "date");
    qs authorId = qs::number(getInt(mes, "from_id"));
    qs peerId = qs::number(getInt(mes, "peer_id"));
    bool out = getInt(mes, "out");
    qs text = getString(mes, "text");
    auto fwd = getArray(mes, "fwd_messages");
    const auto& geo = getValue(mes, "geo");
    const auto& attachments = getArray(mes, "attachments");
    vector<qs> userIds;
    if (auto temp = getValue(mes, "reply_message"); !temp.isNull() && !temp.isUndefined()) fwd = {temp};

    if (auto ac = getValue(mes, "action"); !ac.isNull() && !ac.isUndefined()) //{{{
    {
        auto action = getString(mes, "action.type");
        text += GREATER;

        if (action == "chat_create")
            text += "Создан чат: ";
        else if (action == "chat_title_update")
            text += "Обновил название беседы на: ";
        text += getString(mes, "action.text");

        if (action == "chat_photo_update")
            text += "Обновил фотку беседы";
        else if (action == "chat_photo_remove")
            text += "Удалил фотку беседы";

        if (action == "chat_invite_user")
            text += "Приглашен пользователь";
        else if (action == "chat_invite_user_by_link")
            text += "Пользователь присоединился по ссылке";
        else if (action == "chat_kick_user")
            text += "Исключен пользователь";
        text += LESS;

        auto whoId = getInt(mes, "action.member_id");
        if (whoId)
            userIds = {qs::number(whoId)};
    } //}}}
    vector<message> fwdMessages;
    for (const auto& m : qAsConst(fwd))
        fwdMessages.emplace_back(getMessageFromJson(m));
    message m = message(peerId, text, id, date, authorId, out, std::move(fwdMessages));
    if (!geo.isNull() && !geo.isUndefined())
        m.locations.emplace_back(
            getValue(geo, "coordinates.longitude").toDouble(),
            getValue(geo, "coordinates.latitude").toDouble(),
            getString(geo, "place.title")
        );
    proceedMessageAttachments(m, attachments);
    m.userIds = userIds;
    return m;
} //}}}


void Vk::proceedLongpollUpdate(const QJsonArray& y, bool loadContent) // {{{
{
    int type = y[0].toInt();
    ll messageId = y[1].toInt();
    qs messageIdS = qs::number(messageId);
    switch (type)
    {
        case 1:
            break;
        case 2:
            if (y[2].toInt() & 128)
                writeln("Удалено сообщение", messageId),
                emit onMessageEdited(message(qs::number(y[3].toInt()), "", messageId, 0, qs::number(y[3].toInt()), false, {}, false, true));
            break;
        case 3:
            break;
        case 4:
            [[fallthrough]];
        case 5:
            {
                qs peerId = qs::number(y[3].toInt());
                int date = y[4].toInt();
                qs authorId = peerId;
                bool out = y[2].toInt() & 2;
                if (out) authorId = MY_ID;
                //else if (int fromId = getInt(y[6], "from"); fromId) authorId = fromId;
                else if (qs fromId = getString(y[6], "from"); fromId != "") authorId = fromId;
                qs text = y[5].toString();
                auto attachments = y[7];

                if (!loadContent)
                    break;

                if (getString(attachments, "fwd") != "" || getString(attachments, "attach1_type") != "" || text == "")
                {
                    writeln("Получено/отредактировано сообщение с аттачем или без текста", peerId, messageIdS);
                    emit loadHistory(peerId, -messageId);
                }
                else
                {
                    QTextDocument qqq; qqq.setHtml(text); text = qqq.toPlainText();
                    message m(peerId, text, messageId, date, authorId, out, {}, type == 5);
                    if (type == 5)
                    {
                        writeln("Отредактировано сообщение от", peerId, text);
                        emit onMessageEdited(m);
                    }
                    else
                    {
                        writeln("Получено сообщение от", peerId, text);
                        emit onMessageRecieved(m);
                    }
                }
                break;
            }
        case 6:
            writeln("Прочитаны входящие с", y[2].toInt());
            emit onUserUpdated(messageIdS, y[2].toInt(), -1, -1);
            break;
        case 7:
            writeln("Прочитаны исходящие с", y[2].toInt());
            emit onUserUpdated(messageIdS, -1, y[2].toInt(), -1);
            break;
        case 8:
            break;
        case 9:
            break;
        case 10:
            break;
        case 11:
            break;
        case 12:
            break;
        case 13:
            writeln("Deleting messages from", messageId);
            break;
        case 14:
            writeln("Restoring messages from", messageId);
            break;
        case 51:
            break;
        case 52:
            writeln("В чате какое-то действие");
            emit loadHistory(qs::number(y[2].toInt()), 1);
            break;
        case 61:
            writeln("Юзер печатает", messageId);
            emit userIsTyping(messageIdS, messageIdS);
            break;
        case 62:
            writeln("Юзер печатает", y[2].toInt() + CHAT_OFFSET, messageIdS);
            emit userIsTyping(qs::number(y[2].toInt() + CHAT_OFFSET), messageIdS);
            break;
        case 70:
            break;
        case 80:
            writeln("Количество непрочитанных:", messageId);
            emit onUnreadDialogs(messageId);
            break;
        case 114:
            break;
        default:
            break;
    }
} // }}}

void Vk::longPoll() // {{{
{
    auto x = request("messages.getLongPollServer", {{"lp_version", "3"}, {"need_pts", "1"}});
    if (!x.has_value()) return writeln("Error in getting longpoll server");

    auto res = getValue(x.value(), "response");
    ll ts = getInt(res, "ts");
    ll lastTS = getInt(res, "pts") - 150;

    writeln("Getting longpoll history", lastTS);
    auto historyRes = request("messages.getLongPollHistory", {
        {"lp_version", "3"}, {"onlines", "0"}, {"msgs_limit", "1000"}, {"events_limit", "1000"},
        {"pts", qs::number(lastTS)}
    });
    if (historyRes.has_value())
    {
        auto history = getValue(historyRes.value(), "response");
        proceedProfiles(getArray(history, "profiles"));
        proceedGroups(getArray(history, "groups"));
        const auto& conversations = getArray(history, "conversations");
        int unread = 0;
        for (const auto& c: conversations)
            unread += proceedConversation(c);
        emit onUnreadDialogs(unread);

        const auto& messages = getArray(history, "messages.items");
        for (const auto& m: messages)
            emit onMessageRecieved(getMessageFromJson(m));

        const auto& lp = getArray(history, "history");
        for (const auto& h: lp)
            proceedLongpollUpdate(h.toArray(), false);
    }
    proceedLastDialogs();

    qs serverKey = getString(res, "key");
    qs server = getString(res, "server");

    while (isRunning)
    {
        qs pro = api[4] == 's' ? "https://" : "http://";
        auto actions = network::send(pro + server, {
            {"act", "a_check"}, {"wait", "25"}, {"mode", "2"}, {"version", "3"},
            {"key", serverKey},
            {"ts", qs::number(ts)},
        });
        if (ll x = getInt(actions, "failed"))
            return writeln("Longpoll failed, reason:", x, "restart");

        ts = getInt(actions, "ts");
        const auto& updates = getArray(actions, "updates");
        for (const auto& update: updates)
            proceedLongpollUpdate(update.toArray());
    }
    writeln("Longpoll ended. WTF?");
} //}}}


optional<QJsonObject> Vk::request(const qs& method, map<qs, qs> queryParams, optional<pair<qs, qs>> photo) // {{{
{
    if (queryParams.find("v") == queryParams.end()) queryParams["v"] = version;
    queryParams["access_token"] = key;
    auto response = network::send(api + method, queryParams, std::move(photo));
    if (auto err = getValue(response, "error"); !err.isNull() && !err.isUndefined())
    {
        writeln("Запрос некорректен: ", toString(getValue(response, "error").toObject()));
        return {};
    }
    return response;
} //}}}

void Vk::sendCommand(const QStringList& list, const std::function<void(QJsonObject x)>& callback) // {{{
{
    writeln(list[0]);
    map<qs, qs> params;
    for (int i = 1; i + 1 < list.size(); i += 2)
        params[list[i]] = list[i + 1];
    auto x = request(list[0], params);
    if (x.has_value())
    {
        writeln("Команда успешно выполнена", list.join(" "), x.value());
        callback(x.value());
    }
    else
        writeln("Не удалось выполнить команду:", list.join(" "));
} //}}}

void Vk::startLongPoll() // {{{
{
    writeln("Starting longpoll...");
    if (!isRunning) return writeln("Is not running, stopped");
    delete watcher;
    watcher = new QFutureWatcher<void>();
    connect(watcher, &QFutureWatcher<void>::finished, foo, &Vk::startLongPoll);
    auto f = QtConcurrent::run([&]() {
        try 
        {
            longPoll();
        }
        catch (const exception& e)
        {
            writeln(e.what());
        }
    });
    watcher->setFuture(f);
    writeln("Started longpoll");
} // }}}

void Vk::init(const QJsonValue& config) // {{{
{
    writeln("Init is called");
    key = getString(config, "access_token");
    if (key == "")
        error(R"(No access_token in vk config. Add your private key in ~/.config/vk200/vk.json file (example config):
    {
        "icon": "/home/igorjan/.config/vk200/vk.png",
        "vk": {
            "access_token": "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx",
        },
        "accounts": [{
            "id": 53321,
            "name": "Антон"
        }, {
            "id": 2000000037,
            "name": "Личинка 3D"
        }
    })");

    emit onUserLoaded(user(MY_ID, "Я"));
    proceedLastDialogs();
    startLongPoll();
} //}}}

Vk::Vk(QObject* parent) : // {{{
    QObject(parent)
{
    isRunning = true;

} //}}}

Vk::~Vk() // {{{
{
    isRunning = false;
    delete watcher;
} //}}}

const qs Vk::version = "5.132";
const qs Vk::api = "https://api.vk.com/method/";
//const qs Vk::api = "http://212.109.192.31:8000/";
const qs Vk::name = "Vk";

Vk* Vk::foo = new Vk();
qs Vk::key;
const qs Vk::MY_ID = "3586834";
volatile bool Vk::isRunning;
QFutureWatcher<void>* Vk::watcher;
