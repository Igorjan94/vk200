#include <QProcess>

#include "message.h"
#include "includes.h"
#include "debug.h"
#include "config.h"

message::message() = default;

message::message(qs peerId, qs text, ll id, ll date, qs authorId, bool out, std::vector<message> fwdMessages, bool isEdited, bool isDeleted) :
    peerId(peerId),
    text(text.replace("<3", "❤")),
    id(id),
    authorId(authorId),
    out(out),
    fwdMessages(std::move(fwdMessages)),
    newText(text),
    isEdited(isEdited),
    isDeleted(isDeleted)
{
    if (authorId == "") this->authorId = peerId;
    if (date == 0)
        date = QDateTime::currentMSecsSinceEpoch() / 1000;
    this->date.setSecsSinceEpoch(date);
}

bool message::isRead(ll inRead, ll outRead) const
{
    return isDeleted ? true : id <= (out ? outRead : inRead);
}

bool message::operator<(const message& b) const
{
    return tuple(date, id) < tuple(b.date, b.id);
}

qs message::getText(std::map<qs, qs>& nameById, ll inRead, ll outRead) const
{
    return getHtml(nameById, inRead, outRead).replace("\n", "<br>");
}

qs message::getHtml(std::map<qs, qs>& nameById, ll inRead, ll outRead, int depth) const
{
    cachedText = getCurrentText(nameById, inRead, outRead, depth);
    auto name = nameById[authorId];
    if (topicId != "")
    {
        auto it = nameById.find(peerId + ":" + topicId);
        if (it != nameById.end())
            name = it->second + ": " + name;
    }
    if (forwardedFromId != "")
    {
        auto it = nameById.find(forwardedFromId);
        if (it != nameById.end())
            name = qs(R"(%1 (Сообщение переслано от %2))")
                .arg(name, it->second);
    }
    return qs(R"(<div id="message%8" style="padding-top: 2px; margin-left: %1px; %2"> %3: <a href="https://vk.com/id%9" style="text-decoration: none; color: inherit">%4</a>%5%6 <div style="margin-left: 32px; %2"> %7 </div> </div>)").arg(
        qs::number(32 * !!depth),
        (isRead(inRead, outRead) || depth) ? "" : "background-color: #6897BB; color: black;",
        date.toString(Config::getInstance().DATE_FORMAT),
        name + " " + (erections.size() ? "(+" + erections + ")" : ""),
        isEdited  ? " (edited)" : "",
        isDeleted ? " (deleted)" : "",
        cachedText,
        qs::number(id),
        authorId
    );
}

qs message::getCurrentText(std::map<qs, qs>& nameById, ll inRead, ll outRead, int depth) const
{
    qs curText = text;
    for (const auto& l: links)
        curText = join(curText, l);
    for (const auto& l: locations)
        curText = join(curText, l);
    for (size_t i = 0; i < photos.size(); i += 2)
        if (i + 1 < photos.size())
            curText = join(curText, photos[i] + "&nbsp;" + photos[i + 1]);
        else
            curText = join(curText, photos[i]);

#ifdef __linux__ // {{{
    if (isEdited && newText.size() && text != newText)
    {
        QProcess process;
        process.start("bash", QStringList() << "-c" << "diff -U 10000 <(echo '" + text + "') <(echo '" + newText + "') | tail -n +4");
        process.waitForFinished(-1);
    
        curText = process.readAllStandardOutput();
        curText.remove(size(curText) - 1, 1);
        QStringList list = curText.split("\n");
        for (qs& s: list)
            if (s.startsWith(" "))
                s.remove(0, 1);
            else
                s = qs(R"(<pre style="color: %1; display: inline; font-size: 16">%2</pre>)")
                    .arg(s.startsWith("+") ? "green" : "red", s);

        curText = list.join("\n");
    }
#else
    if (isEdited)
        curText = newText;
#endif // }}}
    if (!fwdMessages.empty()) // {{{
    {
        const qs startSpoiler = "<details><summary>Пересланные сообщения</summary>"; 
        const qs startSpoilerContinueMany = "<details><summary style=\"margin-left: 32px;\">Продолжение пересланных сообщений</summary>"; 
        const qs startSpoilerContinueOne = "<details><summary>Продолжение пересланного сообщения</summary>"; 
        const qs endSpoiler = "</details>";
        const qs startCiting = GREATER;
        const qs endCiting = LESS;
        const int maxLength = 10;
        qs head, tail;
        int linesInHead = 0;
        for (const auto& m : fwdMessages)
        {
            qs text = m.getHtml(nameById, inRead, outRead, depth + 1);
            linesInHead += text.split('\n').size() + 1;
            if (linesInHead <= maxLength)
                head += text;
            else
                tail += text;
        }
        qs fwd;
        if (depth) //Everything in spoiler
            fwd = startSpoiler + startCiting + head + tail +  endCiting + endSpoiler;
        else if (tail.isEmpty()) // Nothing in spoiler
            fwd = startCiting + head + endCiting;
        else if (head.isEmpty())
        {
            if (fwdMessages.size() == 1) // Show first
            {
                QStringList list = tail.split('\n'), first;
                for (int i = 0; i < maxLength / 2; ++i)
                    first += list[i];
                list.erase(list.begin(), list.begin() + maxLength / 2);
                head = first.join('\n');
                tail = list.join('\n');
                fwd = startCiting + head + startSpoilerContinueOne + tail + endSpoiler + endCiting;
            }
            else
                fwd = startSpoiler + startCiting + tail + endCiting + endSpoiler;
        }
        else
            fwd = startCiting + head + startSpoilerContinueMany + tail + endSpoiler + endCiting;
        curText = join(curText, fwd);
    } // }}}
    if (!userIds.empty())
    {
        QStringList names;
        for (const qs& userId: userIds)
            names += nameById[userId];
        curText = join(curText, names.join(", "));
    }
    for (auto match = rx.match(curText); match.hasMatch(); match = rx.match(curText, match.capturedEnd()))
        if (qs first = match.captured(1).split(" ")[0]; tags.find(first.mid(first.size() && first[0] == '/')) == tags.end())
            curText.replace(match.captured(0), match.captured(0).replace("<", "&lt;").replace(">", "&gt;"));
    return curText.replace("+", "&plus;").replace("\\", "&bsol;");
}

const set<qs> message::tags = {"a", "b", "br", "div", "h1", "h2", "h3", "h4", "h5", "h6", "hr", "html", "i", "img", "li", "ol", "p", "pre", "s", "span", "strong", "style", "sub", "sup", "table", "tbody", "td", "th", "tr", "u", "summary", "details", "iframe", "video"};
const QRegularExpression message::rx = QRegularExpression("<(.*?)>");
