#ifndef CONFIG_H
#define CONFIG_H

#include "includes.h"
#include "debug.h"

struct Config
{
private:
    Config() = default;
    Config(const Config&) = delete;
    Config& operator=(Config&) = delete;
public:
    static Config& getInstance()
    {
        static Config foo;
        return foo;
    }

    void load(const QJsonValue& conf)
    {
        if (auto screens = getString(conf, "screenshots_dir"); screens != "")
            SCREENSHOT_DIR = screens;
        if (auto step = getInt(conf, "scroll_step"); step)
            SCROLL_STEP = step;
        if (auto duration = getInt(conf, "scroll_duration"); duration)
            SCROLL_DURATION = duration;
        if (auto countOfMessages = getInt(conf, "count_of_messages"); countOfMessages)
            COUNT_OF_MESSAGES = countOfMessages;
    }

    int SCROLL_DURATION = 8;
    int SCROLL_STEP = 15;
    int COUNT_OF_MESSAGES = 11;
    qs DATE_FORMAT = "dd.MM.yyyy hh:mm";
    qs SCREENSHOT_DIR = "/home/igorjan/screenshots";
};

#endif
