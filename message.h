#ifndef MESSAGE_H
#define MESSAGE_H

#include "includes.h"
#include "debug.h"
static const int MAX_PHOTO_HEIGHT = 404;
static const int MAX_PHOTO_WIDTH = 1000;
static const qs FILE_PREFIX = qs::fromStdString("file://");

struct mylink
{
    qs url;
    qs title;

    mylink(const qs& url, const qs& title) : title(title)
    {
        if (url.startsWith('/'))
            this->url = FILE_PREFIX + url;
        else
            this->url = url;
    }
    mylink() {}

    operator qs() const
    {
        return qs(R"(<a href="%1" style="color: #287BDE;">%2</a>)").arg(url, title);
    }
};

struct photo
{
    qs path;
    qs url;
    int width;
    int height;

    photo(const qs& path, int width = MAX_PHOTO_WIDTH, int height = MAX_PHOTO_HEIGHT) : photo(path, path, width, height) {}

    photo(const qs& path, const qs& url, [[maybe_unused]] int width = MAX_PHOTO_WIDTH, int height = MAX_PHOTO_HEIGHT) : url(url), width(min(width, MAX_PHOTO_WIDTH)), height(min(height, MAX_PHOTO_HEIGHT))
    {
        if (path.startsWith('/'))
            this->path = FILE_PREFIX + path;
        else
            this->path = path;
    }

    operator qs() const
    {
        return mylink(url, qs(R"(<img style="margin-left: 10px" src="%1" height="%2">)").arg(path, qs::number(height)));
    }

    photo() {}
};

struct location
{
    double longitude;
    double latitude;
    qs title;
    photo p;

    location(double longitude, double latitude, const qs& title = "") : longitude(longitude), latitude(latitude), title(title)
    {
        qs image = qs(R"(https://static-maps.yandex.ru/1.x/?ll=%1,%2&size=404,404&z=12&l=map&pt=%1,%2)")
            .arg(longitude).arg(latitude);
        qs full = qs(R"(http://maps.yandex.ru/?ll=%1E,%2N&z=12&l=map&pt=%1E,%2N)")
            .arg(longitude).arg(latitude);
        p = photo(image, full);
    }

    operator qs() const
    {
        return join(title, p);
    }
    
};

class message
{
public:
    message();
    message(qs peerId, qs text, ll id = 0, ll date = 0, qs authorId = "", bool out = false, std::vector<message> fwdMessages = {}, bool isEdited = false, bool isDeleted = false);
    qs getText(std::map<qs, qs>& nameById, ll inRead, ll outRead) const;
    bool operator<(const message& b) const;

    qs peerId;
    qs text;
    ll id;
    QDateTime date;
    qs authorId;
    bool out;
    std::vector<message> fwdMessages;
    std::vector<qs> userIds;
    vector<location> locations;
    vector<photo> photos;
    vector<mylink> links;
    qs topicId = "";
    qs forwardedFromId = "";
    qs erections = "";

    qs newText;
    bool isEdited = false;
    bool isDeleted = false;
    bool isRead(ll inRead, ll outRead) const;
    
private:
    qs getCurrentText(std::map<qs, qs>& nameById, ll inRead, ll outRead, int depth) const;
    qs getHtml(std::map<qs, qs>& nameById, ll inRead, ll outRead, int depth = 0) const;
    static const set<qs> tags;
    static const QRegularExpression rx;
    mutable qs cachedText;
    vector<qs> history;
};

#endif // MESSAGE_H
