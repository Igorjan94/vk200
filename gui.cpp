#include "gui.h"
#include "textedit.h"
#include "ui_vk.h"
#include <thread>
#include <QWebEngineScript>
#include <QWebEngineScriptCollection>

#define CU users[currentUser]

using namespace std;

//SLOTS QT {{{
void Gui::onReturn() // {{{
{
    qs text = ui->lineEdit->text();
    ui->lineEdit->clear();
    if (text == "") return;
    if (text[0] == '/' && text.size() > 1)
    {
        auto spl = qs(text).remove(0, 1).replace("\n", " ").split(" ");
        auto command = spl[0];
        if ((command == "c" || command == "count") && spl.size() >= 2)
        {
            config.COUNT_OF_MESSAGES = spl[1].toInt();
            writeln("Количество загружаемых сообщений", config.COUNT_OF_MESSAGES);
            forceUpdate();
        }
        else if (command == "rename" && spl.size() >= 2)
        {
            spl.erase(spl.begin());
            qs newName = spl.join(" ");
            writeln("Переименовываем пользователя", CU.name, "в", newName);
            CU.name = newName;
            saveConfig();
            setUnread(currentUser, CU.unread);
        }
        else if (command == "mute")
        {
            writeln(vector<qs>({"Muting", "Unmuting"})[CU.muted] + " пользователя", CU.name);
            ui->listWidget->item(indexById(CU.id))->setHidden(!(CU.muted || showMuted));
            CU.muted = !CU.muted;
            saveConfig();
            setUnreadDialogs();
        }
        else if ((command == "u" || command == "user") && spl.size() >= 2)
            CU.social->loadUser(spl[1]);
        else if (command == "delete" || command == "del" || command == "вудуеу" || command == "d")
        {
            if (CU.editedMessage)
            {
                CU.social->deleteMessage(CU.editedMessage.value());
                CU.editedMessage = {};
            }
        }
        else if (command == "erect" || command == "укусе" || command == "e" || command == "е")
        {
            if (CU.editedMessage)
            {
                spl.removeFirst();
                auto reply_to = CU.editedMessage.value().id;
                CU.editedMessage = {};
                CU.social->erect(CU.id, reply_to);
            }
        }
        else if (command == "reply" || command == "куздн" || command == "r" || command == "к")
        {
            if (CU.editedMessage)
            {
                spl.removeFirst();
                auto reply_to = CU.editedMessage.value().id;
                CU.editedMessage = {};
                sendMessage(CU, spl.join(" "), {{"replyTo", reply_to}});
            }
        }
        else if ((command == "switch" || command == "s") && spl.size() >= 2)
        {
            qs peerId = spl[1];
            if (!existsUser(peerId)) CU.social->loadUser(peerId);
            switchToUser(peerId);
        }
        else if (command == "save")
            saveConfig();
        else if (command == "?" || command == "help")
        {
            writeln("a[ttach] <attachment>");
            writeln("c[ount] <countOfMessages>");
            writeln("m");
            writeln("u[ser] <peerId>");
            writeln("e[xecute] <method> <param value>...");
            writeln("f[riends]");
            writeln("o[ffset] <offset>"); 
            writeln("?[help]");
            writeln("s[witch] <userId>");
            writeln("i[mage] <path>...");
            writeln("h[ide]");
        }
        else if (command == "h" || command == "hide")
        {
            bool to = !CU.hidden;
            CU.social->hideUser(CU.id, to);
            hideUser(CU, to);
        }
        else
        {
            if ((command == "image" || command == "i" || command == "doc") && spl.size() >= 2)
                text = text.replace("\\ ", "%20");
            sendMessage(CU, text);
        }
    }
    else
        sendMessage(CU, text);
} //}}}

void Gui::onItemDoubleClicked(QListWidgetItem* item) // {{{
{
    switchToUser(item->data(Qt::UserRole).toString());
} //}}}

void Gui::keyPressEvent(QKeyEvent* event) // {{{
{
    auto getNextUser = [&](bool forward, const function<bool(const user&)>& f) {
        int diff = forward * 2 - 1;
        int len = ui->listWidget->count();
        for (int i = 1; i < len; ++i)
        {
            int j = (currentUser + i * diff + len) % len;
            if (f(users[j]))
                return j;
        }
        return -1;
    };
    auto switchToNextUser = [&](bool forward, const function<bool(const user&)>& f) {
        int index = getNextUser(forward, f);
        if (index == -1) return;
        return switchToUser(ui->listWidget->item(index)->data(Qt::UserRole).toString());
    };
    auto filterUser = [&](const user& u) {
        return (!u.hidden || showHidden) && (!u.muted || showMuted);
    };
    auto filterUnreadUser = [&](const user& u) {
        return filterUser(u) && u.unread;
    };
    auto checkInputIsModified = [&]() {
        qs curText = ui->lineEdit->text();
        
        if (CU.editedMessage)
        {
            const auto& m = CU.editedMessage.value();
            if (qs(curText).replace("\\т", "\n") != m.newText)
                return true;
        }
        else
            if (curText.size())
                return true;
        return false;
    };
    //A-[\d] | C-[\d]
    vector<Qt::Key> keys = {Qt::Key_1, Qt::Key_2, Qt::Key_3, Qt::Key_4, Qt::Key_5, Qt::Key_6, Qt::Key_7, Qt::Key_8, Qt::Key_9, Qt::Key_0};
    if (bool(event->modifiers() & (Qt::ControlModifier | Qt::AltModifier)))
        for (int i = 0, j = 0; i < ui->listWidget->count() && j < int(::size(keys)); ++i)
        {
            if (ui->listWidget->item(i)->isHidden())
                continue;
            if (event->key() == keys[j++])
                return switchToUser(ui->listWidget->item(i)->data(Qt::UserRole).toString());
        }

    //A-*
    if (bool(event->modifiers() & Qt::AltModifier))
        if (event->key() == Qt::Key_Up || event->key() == Qt::Key_Down)
        {
            int i = currentUser;
            int j = getNextUser(event->key() == Qt::Key_Down, filterUser);
            writeln("Moving user", qs::number(i), "to", qs::number(j));
            swap(users[i], users[j]);
            ui->listWidget->item(i)->setData(Qt::UserRole, QVariant(users[i].id));
            ui->listWidget->item(j)->setData(Qt::UserRole, QVariant(users[j].id));
            setUnread(i, users[i].unread);
            setUnread(j, users[j].unread);
            currentUser = j;
            switchToUser(users[j].id);
            saveConfig();
            return;
        }

    //C-S-*
    if (event->modifiers() & Qt::ControlModifier && event->modifiers() & Qt::ShiftModifier)
    {
        if (event->key() == Qt::Key_M)
        {
            writeln(vector<qs>({"Showing", "Hiding"})[showMuted] + " muted users");
            for (const auto& u: users)
                if (u.muted)
                    ui->listWidget->item(indexById(u.id))->setHidden(showMuted);
            showMuted = !showMuted;
            return;
        }
        if (event->key() == Qt::Key_H)
        {
            writeln(vector<qs>({"Showing", "Hiding"})[showHidden] + " hidden users");
            for (const user& u: users)
                if (u.hidden)
                    ui->listWidget->item(indexById(u.id))->setHidden(showHidden);
            showHidden = !showHidden;
            return;
        }
    }

    //S-*
    if (event->modifiers() & Qt::ShiftModifier)
    {
        if (event->key() == Qt::Key_F5)
            return forceUpdate();
    }

    //C-*
    if (event->modifiers() & Qt::ControlModifier)
    {
        if (event->key() == Qt::Key_Q)
        {
            if (checkInputIsModified())
                return;
            ui->lineEdit->setText("/delete");
            return;
        }
        if (event->key() == Qt::Key_R)
        {
            if (checkInputIsModified())
                return;
            ui->lineEdit->setText("/r ");
            return;
        }
        if (event->key() == Qt::Key_E)
        {
            if (checkInputIsModified())
                return;
            ui->lineEdit->setText("/erect ");
            return;
        }
        if (event->key() == Qt::Key_W)
        {
            ui->lineEdit->deleteWord();
            //ui->lineEdit->cursorWordBackward(true);
            //ui->lineEdit->del();
            return;
        }
        if (event->key() == Qt::Key_U)
        {
            user& u = users[currentUser];
            u.social->loadHistory(u.id, config.COUNT_OF_MESSAGES, u.messages.size() ? u.messages[0].id : -1);
            return;
        }
        if (event->key() == Qt::Key_QuoteLeft || event->key() == 1025) //ё-key
            return switchToUser(ui->listWidget->item(previousUser)->data(Qt::UserRole).toString());
        if (event->key() == Qt::Key_Tab || event->key() == Qt::Key_Backtab)
            switchToNextUser(event->key() == Qt::Key_Tab, filterUser);
        if (event->key() == Qt::Key_N || event->key() == Qt::Key_P)
            switchToNextUser(event->key() == Qt::Key_N, filterUnreadUser);
        if (event->key() == Qt::Key_M)
            return markAsRead(CU);
        if (event->key() == Qt::Key_L)
        {
            if (ui->log->isHidden())
                ui->log->show();
            else
                ui->log->hide();
            return;
        }
        if (event->key() == Qt::Key_S)
            ui->lineEdit->setFocus();
        if (event->key() == Qt::Key_G)
        {
            ui->lineEdit->setFocus();
            scrollToBottom(true);
            return;
        }
        if (event->key() == Qt::Key_H)
        {
            bool to = !CU.hidden;
            CU.social->hideUser(CU.id, to);
            hideUser(CU, to);
        }
    }
    if (event->key() == Qt::Key_F12)
    {
        QDir screenshots(config.SCREENSHOT_DIR);
        if (!screenshots.exists())
            return writeln(R"(Directory with screenshots does't exist. Check config's "screenshots_dir" variable)");
        QStringList images = screenshots.entryList(QDir::Files, QDir::Time);
        auto getScr = [&](int image) {
            return config.SCREENSHOT_DIR + QDir::separator() + images[image];
        };
        auto text = ui->lineEdit->text();
        if (text.startsWith("/i")) {
            auto spl = text.split(" ");
            for (int image = 0; image < images.size(); ++image)
            {
                auto app = getScr(image);
                bool found = false;
                for (int i = 1; i < spl.size(); ++i)
                    if (spl[i] == app) 
                        found = true;
                if (!found)
                {
                    spl.insert(spl.begin() + 1, app);
                    ui->lineEdit->setText(spl.join(" "));
                    break;
                }
            }
        } else if (text.startsWith("/"))
            return;
        else {
            auto app = "/i " + getScr(0);
            if (text.size())
                app += " // " + text;
            ui->lineEdit->setText(app);
        }
        return;
    }
    if (event->key() == Qt::Key_Escape)
    {
        setText(CU);
        if (CU.editedMessage.has_value())
        {
            writeln("Отмена редактирования сообщения");
            CU.editedMessage = {};
            ui->lineEdit->clear();
        }
        return;
    }
    if (event->key() == Qt::Key_PageUp || event->key() == Qt::Key_PageDown)
    {
        bool up = event->key() == Qt::Key_PageUp;
        runJS(qs(R"(
            function sleep(ms) {
                return new Promise(resolve => setTimeout(resolve, ms));
            }
            (async () => {
                let step = %2;
                let height = %3;
                for (let i = 0; i < height; i += step)
                {
                    if (%4)
                        window.scrollBy(0, -step);
                    else
                        window.scrollBy(0, step);
                    await sleep(%1);
                }
            })();
        )").arg(qs::number(config.SCROLL_DURATION), qs::number(config.SCROLL_STEP), qs::number(ui->textBrowser->height() / 3), up ? "true" : "false"));
    }
    if (event->key() == Qt::Key_Up || event->key() == Qt::Key_Down)
    {
        int l = int(::size(CU.messages)) - 1;
        int r = 0;
        int d = -1;
        if (event->key() == Qt::Key_Down) swap(l, r), d = 1;
        for (int i = l; i * d <= r * d; i += d)
            if (!CU.messages[i].isDeleted)
            if (CU.messages[i].out || bool(event->modifiers() & Qt::ShiftModifier))
            {
                if (CU.editedMessage && CU.messages[i].id * d <= CU.editedMessage.value().id * d)
                    continue;
                if (checkInputIsModified()) 
                    return;
                CU.editedMessage = CU.messages[i];
                qs temp = CU.messages[i].newText;
                writeln("Редактируем сообщение", temp);
                ui->lineEdit->setText(temp);
                return;
            }
        writeln("Отосланных сообщений в недавних нет!");
        return;
    }
    QWidget::keyPressEvent(event);
} //}}}

void Gui::dropEvent(QDropEvent *event) // {{{
{
    QMimeDatabase db;
    QList<QUrl> urls = event->mimeData()->urls();
    qs type = "i";
    QStringList strings;
    for (const QUrl& url : urls)
    {
        qs path = url.path();
        if (QMimeType mime = db.mimeTypeForFile(path); !mime.name().startsWith("image"))
            type = "doc";
        strings.append(path.replace(" ", "\\ "));
    }
    auto start = "/" + type + " ";
    auto app = strings.join(" ");
    auto text = ui->lineEdit->text();
    if (text.size()) {
        if (text.startsWith("/"))
        {
            auto spl = text.split(" ");
            spl.insert(spl.begin() + 1, app);
            ui->lineEdit->setText(spl.join(" "));
            return;
        } else {
            app += " // " + text;
        }
    }
    ui->lineEdit->setText(start + app);
} //}}}

void Gui::dragEnterEvent(QDragEnterEvent *event) // {{{
{
    event->acceptProposedAction();
} //}}}

void Gui::closeEvent(QCloseEvent *event) // {{{
{
    event->accept();
}// }}}
//}}}

//SLOTS MY {{{
template<typename Social> void Gui::addUser(const user& u) // {{{
{
    if (existsUser(u.id)) return;
    u.social = Social::foo;
    u.type = Social::name.toLower();

    nameById[u.id] = u.name;
    ui->listWidget->addItem(u.name);
    ui->listWidget->item(::size(users))->setData(Qt::UserRole, QVariant(u.id));
    ui->listWidget->item(::size(users))->setHidden(u.hidden || (u.muted && !showMuted));

    users.emplace_back(u);
} //}}}

template<typename Social> void Gui::updateUser(qs peerId, ll inRead, ll outRead, int unread) // {{{
{
    assureUserExists<Social>(peerId);
    int index = indexById(peerId);
    user& curr = users[index];

    if (unread > 0)
    {
        curr.hidden = false;
        ui->listWidget->item(index)->setHidden(false);
        saveConfig();
    }
    if (inRead != -1)
        curr.inRead = inRead;
    if (outRead != -1)
        curr.outRead = outRead;
    updateTextInBrowser(curr, unread);
} // }}}

template<typename Social> void Gui::messageRecieved(const message& m) // {{{
{
    assureUserExists<Social>({m.peerId, m.authorId});
    user& u = users[indexById(m.peerId)];

    auto it = lower_bound(u.messages.begin(), u.messages.end(), m);
    qs tag = "bottom";
    if (it != u.messages.end())
    {
        ll mid = it->id;
        if (mid == m.id)
            return messageEdited<Social>(m);
        tag = "message" + qs::number(mid);
    }
    if (!u.messages.empty() && m.text.length() == 0 && u.messages.back().date == m.date && u.messages.back().authorId == m.authorId)
    {
        auto& lastMessage = u.messages.back();
        for (const auto& x: m.fwdMessages)
            lastMessage.fwdMessages.push_back(x);
        for (const auto& x: m.links)
            lastMessage.links.push_back(x);
        for (const auto& x: m.photos)
            lastMessage.photos.push_back(x);
        for (const auto& x: m.locations)
            lastMessage.locations.push_back(x);
        return messageEdited<Social>(lastMessage);
    }
    else
        u.messages.insert(it, m);

    auto js = qs(R"(
        var cur = document.documentElement.scrollHeight - (window.pageYOffset || document.documentElement.scrollTop) - %3;
        var newDiv = document.createElement("div");
        var before = document.getElementById("%2");
        var parentDiv = before.parentNode;
        parentDiv.insertBefore(newDiv, before);
        newDiv.outerHTML = `%1`;
        if (cur < 50)
            document.getElementById('bottom').scrollIntoView();
    )").arg(m.getText(nameById, u.inRead, u.outRead), tag, qs::number(ui->textBrowser->height()));
    runJS(js, u.id);
    updateTextInBrowser(u);
} //}}}

template<typename Social> void Gui::messageEdited(const message& m) // {{{
{
    assureUserExists<Social>({m.peerId, m.authorId});
    user& u = users[indexById(m.peerId)];
    if (u.messages.empty()) return;
    for (message& root: u.messages)
    {
        function<void(message&)> dfs = [&](message& mm) {
            if (mm.id == m.id)
            {
                if (m.isDeleted)
                    mm.isDeleted = true;
                mm.fwdMessages = m.fwdMessages;
                mm.photos = m.photos;
                mm.links = m.links;
                mm.locations = m.locations;
                mm.erections = m.erections;
                //if (!m.isEdited)
                    //mm.authorId = m.authorId,
                    //mm.date = m.date;
                if (mm.newText.size() == 0 || (!m.isEdited && !m.isDeleted && m.text.size()))
                    mm.text = mm.newText = m.text;
                if (m.isEdited)
                {
                    mm.isEdited = true;
                    if (m.text.size())
                        mm.newText = m.text;
                }
                runJS(
                    qs(R"((document.getElementById("%2") || {}).outerHTML = `%1`;)").arg(
                        root.getText(nameById, u.inRead, u.outRead),
                        "message" + qs::number(root.id)
                    ),
                    u.id
                );
                scrollToBottom();
            }
            for (message& fwd: mm.fwdMessages)
                dfs(fwd);
        };
        dfs(root);
    }
    updateTextInBrowser(u);
} //}}}

void Gui::print(QVariantList list) // {{{
{
    QStringList s;
    for (auto&& x: list) s.append(x.toString());
    qs temp = s.join(" ");

    ui->log->append(temp);
    cout << temp << endl;
    logOfstream << temp << endl;
} //}}}

template<typename Social> void Gui::print(QVariantList list) // {{{
{
    list.insert(0, qs("[%1] [%2]:").arg(Social::name, QDateTime::currentDateTime().toString("hh:mm:ss")));
    print(list);
} //}}}

void Gui::setUnreadDialogs() // {{{
{
    qs curr = ui->listWidget->item(currentUser)->text();
    int dialogs = 0;
    for (const auto& u: users)
        dialogs += !u.isRead();
    qs unread = " [" + qs::number(dialogs) + "]";
    if (dialogs > 1 || (dialogs == 1 && CU.unread == 0))
        curr += unread;
    this->setWindowTitle(curr);
} // }}}

template<typename Social> void Gui::onUserIsTyping(qs peerId, qs userId) // {{{
{
    assureUserExists<Social>({peerId, userId});
    auto now = QDateTime::currentDateTime();
    auto& u = users[indexById(peerId)];
    u.typing[userId] = now;
    writeln(nameById[userId] + " is typing" + (peerId != userId ? " in " + nameById[peerId] : ""), now.toString(config.DATE_FORMAT));
    setTyping(u);
    scrollToBottom();
    QTimer::singleShot(29s, this, [peerId, userId, now, this]() {
        auto& u = users[indexById(peerId)];
        if (u.typing[userId] == now)
            u.typing.erase(userId);
        setTyping(u);
    });
} // }}}

template<typename Social> void Gui::onDraftMessage(const message& m) // {{{
{
    auto& u = users[indexById(m.peerId)];
    if (u.currentText.length() == 0)
    {
        u.currentText = m.text;
        if (CU.id == u.id)
            ui->lineEdit->setText(m.text);
    }
    else
        writeln("Got draft message", m.peerId, m.text);
} //}}}

template<typename Social> void Gui::onUserIsHidden(qs peerId, bool hidden) // {{{
{
    assureUserExists<Social>({peerId});
    if (hidden) // I don't need all this chats here
        hideUser(users[indexById(peerId)], hidden);
} // }}}
//}}}

void Gui::setTyping(const user& u) // {{{
{
    if (u.id != CU.id)
        return;
    qs typing = "";
    QDateTime nnn;
    for (const auto& [userId, now]: CU.typing)
        nnn = now,
        typing += qs("%1, ").arg(nameById[userId]);
    if (typing.size())
    {
        typing.chop(2);
        typing += qs(" печата%1т (%2)").arg(CU.typing.size() > 1 ? "ю" : "е", nnn.toString(config.DATE_FORMAT));
    }
    auto js = qs(R"(
        document.getElementById("bottom").innerHTML = `%1`;
    )").arg(typing);
    runJS(js);
} // }}}

void Gui::sendMessage(user& u, qs text, const map<string, QVariant>& options) // {{{
{
    text.replace("\\n", "\n").replace("\\т", "\n");
    ui->lineEdit->clear();
    if (!u.editedMessage.has_value())
    {
        markAsRead(u);
        u.social->sendMessage(u.id, text, options);
    }
    else
    {
        message m = u.editedMessage.value();
        if (!m.out)
        {
            writeln("Не могу редактировать входящие сообщения :(", text);
            return;
        }
        m.text = m.newText = text;
        u.social->editMessage(m);
    }
    u.editedMessage = {};
    if (u.currentText != "")
        u.social->setDraft(message(u.id, "")),
        u.currentText = "";
} // }}}

void Gui::hideUser(user& u, bool hidden) // {{{
{
    u.hidden = hidden;
    writeln(vector<qs>({"Showing", "Hiding"})[hidden], "пользователя", u.name);
    saveConfig();
    ui->listWidget->item(indexById(u.id))->setHidden(hidden);
} // }}}

void Gui::markAsRead(const user& u) //{{{
{
    vector<ll> messageIds;
    for (const message& m: u.messages)
        if (!m.out && !m.isRead(u.inRead, u.outRead))
            messageIds.push_back(m.id);
    if (messageIds.empty())
        return;
    u.social->markAsRead(u.id, messageIds);
} // }}}

template<typename Social> void Gui::assureUserExists(const vector<qs>& peerIds) // {{{
{
    vector<qs> searched;
    for (const qs& peerId: peerIds)
        if (!existsUser(peerId) && ::find(searched.begin(), searched.end(), peerId) == searched.end())
            searched.emplace_back(peerId);
    if (!searched.empty())
        Social::foo->loadUsers(searched);
}

template<typename Social> void Gui::assureUserExists(qs peerId)
{
    assureUserExists<Social>(vector<qs>(1, peerId));
} // }}}

void Gui::runJS(qs const& js, qs const& peerId) // {{{
{
    if (peerId != "" && peerId != CU.id)
        return;
    ui->textBrowser->page()->runJavaScript(js);
} // }}}

void Gui::scrollToBottom(bool force) // {{{
{
    runJS(qs(R"(
        var cur = document.documentElement.scrollHeight - (window.pageYOffset || document.documentElement.scrollTop) - %1;
        if (cur < 50 || %2)
            document.getElementById('bottom').scrollIntoView();
    )").arg(qs::number(ui->textBrowser->height()), force ? "true" : "false"));
} // }}}

void Gui::setText(user& u) // {{{
{
    if (CU.id != u.id) return;
    qs currentText = "";
    for (auto& m: u.messages)
        currentText += m.getText(nameById, u.inRead, u.outRead);
    currentText += initHtml;
    ui->textBrowser->setHtml(currentText, QUrl("file://"));
    setTyping(u);
} // }}}

void Gui::updateTextInBrowser(user& u, int unread) // {{{
{
    if (CU.id == u.id)
        for (const auto& m: u.messages)
            if (m.id)
                runJS(qs(R"((document.getElementById("%2") || {}).outerHTML = `%1`;)").arg(m.getText(nameById, u.inRead, u.outRead), "message" + qs::number(m.id)));
    if (unread == -1)
    {
        unread = 0;
        for (auto& m: u.messages)
            if (!m.out)
                unread += !m.isRead(u.inRead, u.outRead);
    }
    setUnread(indexById(u.id), unread);
} // }}}

int Gui::indexById(qs peerId) // {{{
{
    for (unsigned i = 0; i < users.size(); ++i)
        if (users[i].id == peerId)
            return i;
    return -1;
} //}}}

template<typename... Tail> void Gui::writeln(Tail... tail) // {{{
{
    print<Gui>(qv(tail...));
} // }}}

void Gui::setUnread(int index, int unread) // {{{
{
    if (unread == -1) return;
    users[index].unread = unread;
    if (unread == 0)
    {
        ui->listWidget->item(index)->setFont(unbold);
        ui->listWidget->item(index)->setText(users[index].name);
        ui->listWidget->item(index)->setHidden((users[index].hidden && !showHidden) || (users[index].muted && !showMuted));
    }
    else
    {
        ui->listWidget->item(index)->setFont(bold);
        ui->listWidget->item(index)->setText(users[index].name + " (" + qs::number(unread) + ")");
        ui->listWidget->item(index)->setHidden(users[index].hidden = users[index].muted && !showMuted);
    }
    setUnreadDialogs();
} //}}}

void Gui::switchToUser(qs peerId) // {{{
{
    int index = indexById(peerId);
    CU.currentText = ui->lineEdit->text();
    CU.social->setDraft(message(CU.id, qs(CU.currentText)));
    if (currentUser != index)
        previousUser = currentUser;
    user& u = users[currentUser = index];
    ui->lineEdit->setText(u.currentText);
    ui->listWidget->item(currentUser)->setSelected(true);
    ui->listWidget->setCurrentRow(currentUser);
    setUnreadDialogs();
    setText(u);
    int messages = ::size(u.messages);
    int lastId = -1;
    if (messages) lastId = u.messages[0].id;
    if (messages < config.COUNT_OF_MESSAGES)
        u.social->loadHistory(u.id, config.COUNT_OF_MESSAGES, lastId);
    writeln("Выбран пользователь: " + u);
} //}}}

void Gui::forceUpdate() // {{{
{
    writeln("Насильная загрузка истории", CU.name, CU.id);
    int messages = ::size(CU.messages);
    int lastId = -1;
    if (messages) lastId = CU.messages.back().id;
    CU.messages.clear();
    CU.messages.resize(0);
    setText(CU);
    this_thread::sleep_for(200ms);
    CU.social->loadHistory(CU.id, config.COUNT_OF_MESSAGES, lastId, true);
} //}}}

bool Gui::existsUser(qs id) // {{{
{
    return indexById(id) != -1;
} //}}}

template<typename Social> void Gui::connectAll(Social* social) // {{{
{
    connect(social, &Social::onUserUpdated,     this, [&](qs peerId, ll inRead, ll outRead, int unread  ) { updateUser<Social>(peerId, inRead, outRead, unread);       });
    connect(social, &Social::onUserLoaded,      this, [&](const user& u                                 ) { addUser         <Social>(u);                               });
    connect(social, &Social::onMessageRecieved, this, [&](message const& m                              ) { messageRecieved <Social>(m);                               });
    connect(social, &Social::onMessageEdited,   this, [&](message const& m                              ) { messageEdited   <Social>(m);                               });
    connect(social, &Social::log,               this, [&](QVariantList list                             ) { print           <Social>(list);                            });
    connect(social, &Social::onUnreadDialogs,   this, [&](                                              ) { setUnreadDialogs        ();                                });
    connect(social, &Social::userIsTyping,      this, [&](qs peerId, qs userId                          ) { onUserIsTyping  <Social>(peerId, userId);                  });
    connect(social, &Social::onDraftMessage,    this, [&](const message& m                              ) { onDraftMessage  <Social>(m);                               });
    connect(social, &Social::onUserHidden,      this, [&](qs peerId, bool hidden                        ) { onUserIsHidden  <Social>(peerId, hidden);                  });
} // }}}

qs getConfigFilePath() // {{{
{
    auto configPath = QStandardPaths::writableLocation(QStandardPaths::ConfigLocation);
    return configPath + QDir::separator() + "vk200" + QDir::separator() + "vk.json";
} // }}}

void Gui::saveConfig() // {{{
{
    //mutex m;
    //unique_lock<mutex> lock(m);
    auto path = getConfigFilePath();
    auto config = loadJson(path).toObject();
    QJsonArray accounts;
    for (const user& u: users)
        accounts.push_back(QJsonValue(u));
    config.insert("accounts", accounts);
    saveJson(QJsonDocument(config), path);
} // }}}

bool Gui::eventFilter(QObject* sender, QEvent* event) // {{{
{
    if (event->type() == QEvent::KeyPress)
    {
        QKeyEvent* keyEvent = static_cast<QKeyEvent*>(event);
        auto our = [&](QKeyEvent* keyEvent) {
            if (sender == ui->lineEdit)
                return (keyEvent->modifiers() & Qt::ControlModifier && (keyEvent->key() == Qt::Key_U || keyEvent->key() == Qt::Key_W))
                    || keyEvent->key() == Qt::Key_PageUp
                    || keyEvent->key() == Qt::Key_PageDown
                    || keyEvent->key() == Qt::Key_Up
                    || keyEvent->key() == Qt::Key_Down
                    || keyEvent->modifiers() & Qt::AltModifier;
            if (sender == ui->listWidget)
                return (keyEvent->key() == Qt::Key_Up || keyEvent->key() == Qt::Key_Down) && (keyEvent->modifiers() & Qt::AltModifier);
            return false;
        };
        if (our(keyEvent))
        {
            keyPressEvent(keyEvent);
            return true;
        }
    };
    return QWidget::eventFilter(sender, event);
} // }}}

Gui::Gui(QWidget *parent) : // {{{
    QWidget(parent),
    ui(new Ui::Gui)
{
    auto cachePath = QStandardPaths::writableLocation(QStandardPaths::CacheLocation);
    qs filename = cachePath + QDir::separator() + "log" + QDateTime::currentDateTime().toString("yyyy-MM-dd.hh:mm:ss");
    logOfstream = ofstream(filename.toStdString());

    qRegisterMetaType<message>("message");
    qRegisterMetaType<user>("user");
    qRegisterMetaType<ll>("ll");
    qRegisterMetaType<QVariantList>("QVariantList");
    ui->setupUi(this);
    //ui->textBrowser->setOpenExternalLinks(true);
    //ui->textBrowser->copyAvailable(true);

    auto conf = loadJson(getConfigFilePath());

    auto font = ui->listWidget->font();
    auto fsz_s = getString(conf, "fontsize");
    int fsz = fsz_s == "" ? 16 : fsz_s.toInt();
    font.setPointSize(fsz / 4 * 3);
    ui->log->setFont(font);
    ui->lineEdit->setFont(font);
    ui->listWidget->setFont(font);

    QWebEngineSettings *defaultSettings = QWebEngineProfile::defaultProfile()->settings();
    defaultSettings->setFontSize(QWebEngineSettings::DefaultFontSize, fsz);
    defaultSettings->setFontFamily(QWebEngineSettings::StandardFont, font.family());
    defaultSettings->setAttribute(QWebEngineSettings::PluginsEnabled, true);
    defaultSettings->setAttribute(QWebEngineSettings::DnsPrefetchEnabled, true);

    ui->log->hide();
    ui->splitter->setSizes({200, size().width() - 400, 200});

    ui->textBrowser->setContextMenuPolicy(Qt::ContextMenuPolicy::DefaultContextMenu);
    ui->textBrowser->setFocusPolicy(Qt::FocusPolicy::NoFocus);
    ui->textBrowser->setHtml(initHtml);

    ui->lineEdit->installEventFilter(this);
    ui->listWidget->installEventFilter(this);

    //ui->listWidget->setDragDropMode(QAbstractItemView::InternalMove);
    //ui->listWidget->setDefaultDropAction(Qt::MoveAction);
    bold.setBold(true);
    unbold.setBold(false);
    setAcceptDrops(true);

    connect(network::foo, &network::docIsLoaded, this, [&]() { updateTextInBrowser(CU); });
    connect(network::foo, &network::log, this, [&](QVariantList list) { print<network>(list); });
    connect(ui->textBrowser, &QWebEngineView::loadFinished, this, &Gui::scrollToBottom);

    config.load(conf);
    if (auto icon = getString(conf, "icon"); icon != "")
        setWindowIcon(QIcon(icon));
    if (auto corpus = getString(conf, "corpus"); corpus != "")
        ui->lineEdit->setQuantities(loadJson(corpus), fsz);

    if (auto accounts = getArray(conf, "accounts"); !accounts.empty())
        for (auto account: accounts)
        {
            user u(account);
            if (qs temp = getString(account, "type"); temp == "vk" || temp == "")
                addUser<Vk>(u);
#ifdef TELEGRAM
            else if (temp == "tg")
                addUser<Telegram>(u);
#endif
        }
#define initSocial(Social) if (auto socialConf = getValue(conf, Social::name.toLower()); true) \
    { \
        connectAll<Social>(Social::foo); \
        Social::foo->init(socialConf); \
    }
    initSocial(Vk);
#ifdef TELEGRAM
    initSocial(Telegram);
#endif
#undef initSocial

    for (int i = 0; i < ui->listWidget->count(); ++i)
        if (!ui->listWidget->isHidden())
        {
            switchToUser(ui->listWidget->item(i)->data(Qt::UserRole).toString());
            break;
        }
    QWebEngineScript script;

    qs s = R"(
        var css = document.createElement('style');
        css.type = 'text/css';
        document.head.appendChild(css);
        css.innerText = `
            ::-webkit-scrollbar {
                color: #A9B7C6;
                width: 12px;
            }

            ::-webkit-scrollbar-thumb {
                background: #484A4A;
                border-radius: 10px;
            }
        `;
    )";
    runJS(s);
    script.setSourceCode(s);
    script.setInjectionPoint(QWebEngineScript::DocumentReady);
    script.setRunsOnSubFrames(true);
    script.setWorldId(QWebEngineScript::ApplicationWorld);
    ui->textBrowser->page()->scripts().insert(script);
} // }}}

Gui::~Gui()
{
    logOfstream.close();
    delete ui;
    delete network::foo;
    delete Vk::foo;
#ifdef TELEGRAM
    delete Telegram::foo;
#endif
}

const qs Gui::name = "Gu";
const qs Gui::initHtml = R"(<body style="background-color: #2B2B2B; color: #A9B7C6;"><div id="bottom"></div></body>)";
Config& Gui::config = Config::getInstance();
