#include "textedit.h"
#include "debug.h"

TextEdit::TextEdit(QWidget *parent)
    : QTextEdit(parent)
{
    setTabChangesFocus(true);
    setWordWrapMode(QTextOption::NoWrap);
    setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);
    setAcceptRichText(false);
}

TextEdit::~TextEdit() {}

void TextEdit::setQuantities(const QJsonValue& corpus, int fsz)
{
    QStringList longPhrases[2];
    QStringList middlePhrases[2];
    QStringList shortPhrases[2];
    QStringList singleOccurencies;
    const auto& m = corpus.toObject().toVariantMap().toStdMap();
    for (const auto& [key, value]: m)
    {
        auto c = value.toLongLong();
        int l = key.length();
        if (c < 2) {
            if (l < 7) continue;
            singleOccurencies << key;
        }
        else if (l >= 20)
            longPhrases[c >= 5] << key;
        else if (l >= 10)
            middlePhrases[c >= 10] << key;
        else
            shortPhrases[c >= 15] << key;
    }
    auto addCompleter = [&](const QStringList& words) {
        QCompleter* c = new QCompleter(words, this);
        c->setWrapAround(false);
        c->setMaxVisibleItems(3);
        c->setWidget(this);
        c->setCompletionMode(QCompleter::PopupCompletion);
        c->setCaseSensitivity(Qt::CaseInsensitive);
        auto font = c->popup()->font();
        font.setPointSize(fsz / 4 * 3);
        c->popup()->setFont(font);
        QObject::connect(c, QOverload<const qs&>::of(&QCompleter::activated), this, &TextEdit::insertCompletion);
        completers.push_back(c);
    };
    for (int i = 1; i >= 0; --i)
    {
        addCompleter(longPhrases[i]);
        addCompleter(middlePhrases[i]);
        addCompleter(shortPhrases[i]);
    }
    addCompleter(singleOccurencies);
}

void TextEdit::deleteWord()
{
    QTextCursor tc = textCursor();
    tc.movePosition(QTextCursor::WordLeft, QTextCursor::KeepAnchor);
    tc.insertText("");
}

qs TextEdit::text() const
{
    return toPlainText();
}

bool TextEdit::hideCompleter(bool condition)
{
    if (currentCompleter)
        if (condition || text() == "" || text().endsWith("\\т") || text().endsWith("\\n"))
        {
            currentCompleter->popup()->hide();
            currentCompleter = nullptr;
            return true;
        }
    return false;
}

void TextEdit::insertCompletion(const qs &completion)
{
    if (!currentCompleter) return;
    QTextCursor tc = textCursor();
    int extra = completion.length() - currentCompleter->completionPrefix().length();
    qs t = completion.right(extra);
    t = t.left(t.indexOf(' ', 1));
    tc.insertText(t + " ");
    setTextCursor(tc);
    complete();
}

vector<qs> TextEdit::textUnderCursor() const
{
    vector<qs> texts;
    QTextCursor tc = textCursor();
    for (int i = 0; i < 3; ++i)
    {
        tc.movePosition(QTextCursor::WordLeft, QTextCursor::KeepAnchor);
        qs t = tc.selectedText();
        if (t.contains("\\т") || t.contains("\\n") || t == "")
            break;
        if (t.startsWith("т") || t.startsWith("n"))
        {
            tc.movePosition(QTextCursor::WordLeft, QTextCursor::KeepAnchor);
            if (tc.selectedText().contains('\\'))
            {
                texts.push_back(t.right(t.size() - 1));
                break;
            }
        }
        texts.push_back(t);
    }
    reverse(texts.begin(), texts.end());
    return texts;
}

void TextEdit::setText(const qs& text)
{
    QTextEdit::setText(qs(text).replace("\n", "\\т"));
    QTextCursor tc = textCursor();
    tc.movePosition(QTextCursor::End);
    setTextCursor(tc);
}

void TextEdit::focusInEvent(QFocusEvent *e)
{
    if (currentCompleter) currentCompleter->setWidget(this);
    QTextEdit::focusInEvent(e);
}

void TextEdit::keyPressEvent(QKeyEvent *e)
{
    if (completers.empty()) 
        return QTextEdit::keyPressEvent(e);
    if (currentCompleter && currentCompleter->popup()->isVisible()) {
        switch (e->key()) {
            case Qt::Key_Enter:
            case Qt::Key_Return:
            case Qt::Key_Escape:
            case Qt::Key_Tab:
            case Qt::Key_Backtab:
                e->ignore();
                return;
            default:
                break;
        }
    }
    if (e->key() == Qt::Key_Return || e->key() == Qt::Key_Enter)
    {
        emit returnPressed();
        return e->ignore();
    }

    QTextEdit::keyPressEvent(e);

    if (e->text().isEmpty())
    {
        hideCompleter(true);
        return;
    }

    static qs eow("~!@#$%^&*()_+{}|:\"<>?,./;'[]\\-="); // end of word
    if (qs("~!@#$%^&*_|:?,./;)]>\\").contains(e->text()))
    {
        qs t = text();
        if (t.size() >= 2 && t[t.size() - 2] == ' ')
            setText(t.left(t.size() - 2) + t.back());
    }
    hideCompleter(eow.contains(e->text().right(1)));
    complete();
}

void TextEdit::complete()
{
    const auto& completionPrefix = textUnderCursor();

    for (const qs& prefix: completionPrefix)
        for (auto* c: completers)
        {
            c->setCompletionPrefix(prefix);
            if (c->completionCount())
            {
                hideCompleter(currentCompleter != c);
                QRect cr = cursorRect();
                c->popup()->setCurrentIndex(c->completionModel()->index(0, 0));
                cr.setWidth(c->popup()->sizeHintForColumn(0) + c->popup()->verticalScrollBar()->sizeHint().width());
                c->complete(cr);
                currentCompleter = c;
                hideCompleter(false);
                return;
            }
        }
    hideCompleter(true);
}
