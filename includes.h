#ifndef INCLUDES_H
#define INCLUDES_H

//Когда-нибудь, в следующей жизни, я избавлюсь от этой привычки
#include <bits/stdc++.h>

#include <QAbstractItemView>
#include <QCompleter>
#include <QDateTime>
#include <QDir>
#include <QEventLoop>
#include <QFileInfo>
#include <QFontDatabase>
#include <QFutureWatcher>
#include <QHttpMultiPart>
#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonObject>
#include <QKeyEvent>
#include <QListWidget>
#include <QMimeData>
#include <QMimeDatabase>
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QNetworkRequest>
#include <QRegularExpression>
#include <QScrollBar>
#include <QSize>
#include <QStandardPaths>
#include <QTextDocument>
#include <QTextEdit>
#include <QTextStream>
#include <QTimer>
#include <QUrlQuery>
#include <QWebEngineProfile>
#include <QWebEngineSettings>
#include <QWebEngineView>
#include <QWidget>
#include <QtConcurrent>

#include <td/telegram/Client.h>
#include <td/telegram/td_api.h>
#include <td/telegram/td_api.hpp>

#define qs QString
#define strToQs QString::fromStdString
#define qsToStr QString::toStdString
#define TAB "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
#define GREATER "&gt;&gt;&gt;&gt;&gt;"
#define LESS "&lt;&lt;&lt;&lt;&lt;"

typedef long long ll;

using namespace std;

#endif // INCLUDES_H
