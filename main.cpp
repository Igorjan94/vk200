#include "gui.h"

#include <QApplication>

int main(int argc, char *argv[])
{
    qputenv("QT_XCB_FORCE_SOFTWARE_OPENGL", "1");
    char ARG_DISABLE_WEB_SECURITY[] = "--disable-web-security";
    int newArgc = argc + 2;
    char** newArgv = new char* [newArgc];
    for(int i = 0; i < argc; i++)
        newArgv[i] = argv[i];
    newArgv[argc] = ARG_DISABLE_WEB_SECURITY;
    newArgv[argc + 1] = nullptr;
    QApplication a(newArgc, newArgv);
    Gui gui;
    gui.setAttribute(Qt::WA_DeleteOnClose);
    gui.show();

    return a.exec();
}
