#ifndef TEXTEDIT_H
#define TEXTEDIT_H

#include "includes.h"

class TextEdit : public QTextEdit
{
    Q_OBJECT

public:
    TextEdit(QWidget *parent = nullptr);
    ~TextEdit();

    void setQuantities(const QJsonValue& corpus, int fsz);
    void deleteWord();
    void setText(const qs& text);
    qs text() const;

protected:
    void focusInEvent(QFocusEvent *e);

public slots:
    void insertCompletion(const qs &completion);
    void keyPressEvent(QKeyEvent *e);

signals:
    void returnPressed();

private:
    vector<qs> textUnderCursor() const;
    void complete();
    vector<QCompleter*> completers;
    QCompleter* currentCompleter = nullptr;
    bool hideCompleter(bool condition);
};

#endif //TEXTEDIT_H
