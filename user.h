#ifndef USER_H
#define USER_H

#include "includes.h"
#include "message.h"
class SocialInterface;

struct user
{
    qs id;
    qs name;

    qs currentText;

    std::optional<message> editedMessage;
    long long inRead = 0;
    long long outRead = 0;
    bool hidden = true;
    bool muted = false;
    int unread = 0;
    mutable SocialInterface* social;
    mutable qs type;

    std::vector<message> messages;
    map<qs, QDateTime> typing;

    user();
    user(QJsonValue);
    user(qs id, const qs& name, bool hidden = true, bool muted = false);
    user(int id, const qs& name, bool hidden = true, bool muted = false);

    operator qs();
    operator QJsonObject() const;
    bool isRead() const;
    void addMessage(const message& m);
};

#endif
