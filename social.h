#ifndef SOCIAL_H
#define SOCIAL_H

#include "debug.h"
#include "network.h"
#include "includes.h"
#include "message.h"
#include "user.h"

class SocialInterface
{
public:
    virtual ~SocialInterface() = default;
    static const qs name;

    virtual void loadHistory  (qs peerId, int count, long long startMessageId = -1, bool force = false) = 0;
    virtual void loadUser     (qs peerId) = 0;
    virtual void loadUsers    (const std::vector<qs>& peerIds) = 0;
    virtual void markAsRead   (qs peerId, const vector<ll>& messageIds) = 0;
    virtual void sendMessage  (qs peerId, qs const& text, map<string, QVariant> options = {}) = 0;
    virtual void deleteMessage(message const& m) = 0;
    virtual void editMessage  (message const& m) = 0;
    virtual void setDraft     (message const& m) = 0;
    virtual void hideUser     (qs peerId, bool hidden) = 0;
    virtual void erect        (qs peerId, ll messageId) = 0;

    virtual void init(const QJsonValue& config) = 0;
public slots:
    virtual void startLongPoll() = 0;
signals:
    void onUserLoaded(user const& u);
    void onUserUpdated(qs peerId, long long inRead, long long outRead, int unread);
    void onMessageRecieved(message const& m);
    void onMessageEdited(message const& m);
    void log(const QVariantList& list);
    void onUnreadDialogs(int dialogs);
    void userIsTyping(qs peerId, qs userId);
    void onDraftMessage(const message& m);
    void onUserHidden(qs peerId, bool hidden);
};

#endif //SOCIAL_H

