#include "telegram.h"
#include <td/telegram/td_api.h>
#undef emit
#define emit foo->

template<typename T>
function<void(Object)> Telegram::errorHandler(const qs& title, const function<void(td_api::object_ptr<T>)>& f) //{{{
{
    return [title, f](Object object) {
        if (object->get_id() == td_api::error::ID) {
            auto error = move_tl_object_as<td_api::error>(object);
            writeln("Error:", title, qs::fromStdString(to_string(error)));
        }
        if constexpr (!is_same_v<T, int>)
            f(move_tl_object_as<T>(object));
    };
} //}}}

//SLOTS {{{
void Telegram::loadHistory(qs peerId, int count, ll startMessageId, bool force) //{{{
{
    auto handler = [](td_api::object_ptr<td_api::messages> messages) {
        if (!messages)
            return;
        reverse(messages->messages_.begin(), messages->messages_.end());
        for (const auto& m: messages->messages_)
        {
            const auto& state = std::move(m->sending_state_);
            if (!state || (state->get_id() != td_api::messageSendingStatePending::ID && state->get_id() != td_api::messageSendingStateFailed::ID))
                emit onMessageRecieved(getMessageFromApi(*m));
        }
    };

    const auto threadId = splitPeer(peerId);
    if (threadId.size())
    {
        if (startMessageId == -1) return; //Cannot get thread history without message id. TODO: update when updated
        auto get_history = td_api::make_object<td_api::getMessageThreadHistory>();
        get_history->chat_id_ = peerId.toLongLong();
        get_history->limit_  = count;
        if (startMessageId > 0)
        {
            get_history->message_id_ = startMessageId;
            if (!force)
                get_history->from_message_id_ = startMessageId;
        }
        send_query(std::move(get_history), errorHandler<td_api::messages>("Get thread history", handler));
    }
    else
    {
        auto get_history = td_api::make_object<td_api::getChatHistory>();
        get_history->chat_id_ = peerId.toLongLong();
        get_history->limit_  = count;
        if (startMessageId > 0 && !force)
            get_history->from_message_id_ = startMessageId;
        send_query(std::move(get_history), errorHandler<td_api::messages>("Get history", handler));
    }
} //}}}

void Telegram::loadUser(qs peerId) //{{{
{
    loadUsers({peerId});
}
//}}}

void Telegram::loadUsers([[maybe_unused]] const vector<qs>& peerIds) //{{{
{
} //}}}

void Telegram::markAsRead(qs peerId, const vector<ll>& messageIds) // {{{
{
    writeln("Marking user as read", peerId);
    splitPeer(peerId);
    auto mark = td_api::make_object<td_api::viewMessages>();
    mark->chat_id_ = peerId.toLongLong();
    for (const ll& id: messageIds)
        if (id)
            mark->message_ids_.push_back(id);
    mark->force_read_ = true;
    send_query(std::move(mark), errorHandler("Mark as read"));
} //}}}

void Telegram::editOrSendMessage(qs peerId, qs text, const map<string, QVariant>& options) // {{{
{
    qs attachments = "";
    if (options.find("attachment") != options.end())
        attachments = options.at("attachment").toString();
    writeln("Sending message", text, attachments, "to", peerId);
    auto message_content = td_api::make_object<td_api::inputMessageText>();
    message_content->text_ = td_api::make_object<td_api::formattedText>();
    message_content->text_->text_ = text.toStdString();
    const auto threadId = splitPeer(peerId);
    if (options.find("messageId") == options.end())
    {
        auto send_message = td_api::make_object<td_api::sendMessage>();
        if (options.find("replyTo") != options.end())
        {
            auto reply = td_api::make_object<td_api::inputMessageReplyToMessage>();
            reply->message_id_ = options.at("replyTo").toLongLong();
            send_message->reply_to_ = std::move(reply);
        }
        send_message->chat_id_ = peerId.toLongLong();
        send_message->input_message_content_ = std::move(message_content);
        if (threadId.size())
            send_message->message_thread_id_ = threadId.toLongLong();
        send_query(std::move(send_message), errorHandler("Send message"));
    }
    else
    {
        auto editMessage = td_api::make_object<td_api::editMessageText>();
        editMessage->chat_id_ = peerId.toLongLong();
        editMessage->message_id_ = options.at("messageId").toLongLong();
        editMessage->input_message_content_ = std::move(message_content);
        send_query(std::move(editMessage), errorHandler("Edit message"));
    }
} // }}}

void Telegram::sendMessage(qs peerId, const qs& text, map<string, QVariant> options) // {{{
{
    if (text.size() == 0) return;
    if (text[0] != '/')
        editOrSendMessage(peerId, text, options);
    else
    {
        auto raw = qs(text).remove(0, 1);
        auto spl = qs(raw).replace("\n", " ").split(" ");
        auto command = spl[0];
        if ((command == "image" || command == "i" || command == "doc") && spl.size() >= 2)
        {
            auto capSpl = raw.split("//");
            spl = capSpl[0].split(" ");
            spl.removeAt(0);
            capSpl.removeAt(0);
            auto caption = td_api::make_object<td_api::formattedText>();
            caption->text_ = capSpl.join("//").toStdString();
            writeln("Sending", command, spl.join(" "));
            for (auto& doc: spl)
                if (doc.size())
            {
                doc = doc.replace("%20", " ");
                auto send_message = td_api::make_object<td_api::sendMessage>();
                send_message->chat_id_ = peerId.toLongLong();
                if (command == "doc")
                {
                    auto document = td_api::make_object<td_api::inputMessageDocument>();
                    document->document_ = td_api::make_object<td_api::inputFileLocal>(doc.toStdString());
                    document->caption_ = std::move(caption);
                    send_message->input_message_content_ = std::move(document);
                }
                else
                {
                    auto photo = td_api::make_object<td_api::inputMessagePhoto>();
                    photo->photo_ = td_api::make_object<td_api::inputFileLocal>(doc.toStdString());
                    photo->caption_ = std::move(caption);
                    send_message->input_message_content_ = std::move(photo);
                }
                send_query(std::move(send_message), errorHandler("Send photo"));
            }
        }
        else
            editOrSendMessage(peerId, text, options);
    }
} //}}}

void Telegram::deleteMessage(const message& m) //{{{
{
    auto deleteMessage = td_api::make_object<td_api::deleteMessages>();
    deleteMessage->chat_id_ = m.peerId.toLongLong();
    deleteMessage->message_ids_ = {m.id};
    deleteMessage->revoke_ = true;
    send_query(std::move(deleteMessage), {});
} // }}}

void Telegram::erect(qs peerId, ll messageId) //{{{
{
    writeln("Erecting message", peerId, qs::number(messageId));
    splitPeer(peerId);
    auto emoji = td_api::make_object<td_api::reactionTypeEmoji>();
    emoji->emoji_ = "👍";
    auto e = td_api::make_object<td_api::addMessageReaction>();
    e->chat_id_ = peerId.toLongLong();
    e->message_id_ = messageId;
    e->reaction_type_ = std::move(emoji);
    e->is_big_ = false;
    send_query(std::move(e), errorHandler("Erect message"));
} // }}}

void Telegram::editMessage(const message& m) //{{{
{
    editOrSendMessage(m.peerId, m.text, {{"messageId", m.id}});
} // }}}

void Telegram::setDraft(const message& m) //{{{
{
    auto message_content = td_api::make_object<td_api::inputMessageText>();
    message_content->text_ = td_api::make_object<td_api::formattedText>();
    message_content->text_->text_ = m.text.toStdString();

    auto draft_message = td_api::make_object<td_api::draftMessage>();
    draft_message->date_ = QDateTime::currentDateTime().currentSecsSinceEpoch();
    draft_message->input_message_text_ = std::move(message_content);

    auto peerId = m.peerId;
    splitPeer(peerId);

    auto set_draft = td_api::make_object<td_api::setChatDraftMessage>();
    set_draft->chat_id_ = peerId.toLongLong();
    set_draft->draft_message_ = std::move(draft_message);
    send_query(std::move(set_draft), errorHandler("Set draft message"));
} // }}}

void Telegram::hideUser(qs peerId, bool hidden) // {{{
{
    return; //Hiding here does not mean we want to archive chat
    auto setChatChatList = td_api::make_object<td_api::addChatToList>();
    setChatChatList->chat_id_ = peerId.toLongLong();
    if (hidden)
        setChatChatList->chat_list_ = td_api::make_object<td_api::chatListArchive>();
    else
        setChatChatList->chat_list_ = td_api::make_object<td_api::chatListMain>();
    send_query(std::move(setChatChatList), errorHandler("Hiding user"));
} // }}}

void Telegram::print(const QVariantList& list) // {{{
{
    return;
    for (const auto& x: list)
        cout << x.toString() << " ";
    cout << endl;
} //}}}

template<typename... Tail> void Telegram::writeln(const Tail&... tail) // {{{
{
    print(qv(tail...));
    emit log(qv(tail...));
} // }}}
//}}}

qs Telegram::splitPeer(qs& peerId)
{
    return "";
    const auto spl = peerId.split(":");
    if (spl.size() == 2)
    {
        peerId = spl[0];
        return spl[1];
    }
    return "";
}

void getImg(message& ret, const string& path, const int& height, const string& filename) { // {{{
    if (height == 0) // It is document
    {
        ret.links.emplace_back(qs::fromStdString(path), qs::fromStdString(filename));
        return;
    }
    if (auto temp = qs::fromStdString(path).toLower(); temp.endsWith("mp4") || temp.endsWith("mov")) // It is video
    {
        ret.text = join(ret.text, (qs(R"(<video controls="controls" style="margin-left: 10px" height="%2" src="file://%1"></video>)")
            .arg(qs::fromStdString(path))
            .arg(qs::number(min(404, height)))));
        return;
    }

    // It is photo
    ret.photos.emplace_back(qs::fromStdString(path), 0, height);
} // }}}

size_t findLargestPhoto(const td_api::object_ptr<td_api::photo>& photos) { // {{{
    int mx = 0;
    unsigned index = 0;
    for (size_t i = 0; i < photos->sizes_.size(); ++i)
    {
        const auto& p = photos->sizes_[i];
        if (int temp = p->height_ + p->width_; temp > mx)
            mx = temp,
            index = i;
    }
    return index;
} // }}}

void Telegram::getFile(message& ret, const td_api::file& p, const int& height, const string& filename) { // {{{
    if (p.local_ && p.local_->path_.size())
        getImg(ret, p.local_->path_, height, filename);
    else
    {
        auto get = td_api::make_object<td_api::downloadFile>();
        get->file_id_ = p.id_;
        get->priority_ = 1;
        get->synchronous_ = true;
        ret.text = join("Photo/doc is loading...", ret.text);
        files[p.id_] = tuple(ret, height, filename);
        send_query(std::move(get), errorHandler<td_api::file>("Get file", [](td_api::object_ptr<td_api::file> file) {
            if (file && file->local_ && file->local_->path_.size())
                if (auto it = files.find(file->id_); it != files.end())
                {
                    auto& [m, photoSize, fn] = (*it).second;
                    m.text.replace("Photo/doc is loading...", "");
                    if (m.text.size() && m.text[0] == '\n') m.text = m.text.mid(1);
                    emit onMessageEdited(m);
                    getImg(m, file->local_->path_, photoSize, fn);
                    emit onMessageEdited(m);
                    files.erase(it);
                }
        }));
    }
} // }}}

void Telegram::setMessageText(message& ret, const td_api::MessageContent&& content) // {{{
{
    auto append = [&ret](string s) { // {{{
        ret.text = join(ret.text, qs::fromStdString(s));
    };
    auto getText = [&](const td_api::object_ptr<td_api::formattedText>& text) {
        ret.text = qs::fromStdString(text->text_);
        auto insertTag = [&](int offset, int length, qs tag, qs style) {
            if (tag.isEmpty()) return;
            ret.text.insert(offset + length, qs(R"(</%1>)").arg(tag));
            ret.text.insert(offset, qs(R"(<%1 style="display: inline; %2">)").arg(tag, style));
        };
        for (int i = int(text->entities_.size()) - 1; i >= 0; --i)
        {
            auto&& entity = text->entities_[i];
            qs tag = "";
            qs style = "";
            qs value = ret.text.mid(entity->offset_, entity->length_);
            auto type = entity->type_->get_id();
            if (type == td_api::textEntityTypeItalic::ID)
                tag = "i";
            else if (type == td_api::textEntityTypeBold::ID)
                tag = "b";
            else if (type == td_api::textEntityTypePre::ID)
                tag = "pre";
            else if (type == td_api::textEntityTypeCode::ID)
                tag = "span",
                style = "padding: 2px 4px; color: #c7254e; background-color: #f9f2f4; border-radius: 4px;";
            else if (type == td_api::textEntityTypePreCode::ID)
                tag = "pre";
            else if (type == td_api::textEntityTypeMention::ID)
                tag = "b",
                style = "background-color: #add8e6";
            else if (type == td_api::textEntityTypeUrl::ID)
                tag = "a";
            insertTag(entity->offset_, entity->length_, tag, style);
            if (tag == "a")
                ret.text.insert(entity->offset_ + 3, qs(R"(href="%1")").arg(value));
        }

    }; /*}}}*/

    if (content.get_id() == td_api::messageText::ID)
    {
        auto&& c = static_cast<const td_api::messageText &>(content);
        getText(c.text_);
        if (c.web_page_)
        {
            ret.links.emplace_back(qs::fromStdString(c.web_page_->url_), qs::fromStdString(c.web_page_->title_));
            append(c.web_page_->description_->text_);
            if (c.web_page_->photo_)
            {
                auto&& photos = c.web_page_->photo_;
                auto&& photo = photos->sizes_[findLargestPhoto(photos)];
                getFile(ret, *photo->photo_, photo->height_);
            }
        }
    }
    if (content.get_id() == td_api::messageInviteVideoChatParticipants::ID) 
    {
        auto&& userIds = static_cast<const td_api::messageInviteVideoChatParticipants &>(content).user_ids_;
        for (const auto& userId: userIds)
            ret.userIds.push_back(qs::number(userId));
        append(GREATER "Пригласили в голосовой чат" LESS);
    }
    if (content.get_id() == td_api::messageChatJoinByLink::ID)
        append(GREATER "Присоединился к чату по ссылке" LESS);
    if (content.get_id() == td_api::messageChatAddMembers::ID)
    {
        auto&& userIds = static_cast<const td_api::messageChatAddMembers &>(content).member_user_ids_;
        for (const auto& userId: userIds)
            ret.userIds.push_back(qs::number(userId));
        append(GREATER "Пригласил в этот чат" LESS);
    }
    if (content.get_id() == td_api::messageChatDeleteMember::ID)
    {
        auto&& userId = static_cast<const td_api::messageChatDeleteMember &>(content).user_id_;
        ret.userIds = {qs::number(userId)};
        append(GREATER "Удалил из этого чата" LESS);
    }
    if (content.get_id() == td_api::messagePoll::ID)
    {
        auto&& poll = static_cast<const td_api::messagePoll &>(content).poll_;
        vector<QStringList> table;
        for (const auto& answer: poll->options_)
            table.push_back({
                qs::fromStdString(answer->text_),
                qs::number(answer->voter_count_),
                qs::number(answer->vote_percentage_) + "%",
                answer->is_chosen_ ? "✓" : ""
            });

        qs pollString = tableView("Опрос: " + qs::fromStdString(poll->question_), "<pre style=\"margin-left: 32px; display: inline\">%1  %2  (%3) %4</pre>", table);
        append(pollString.toStdString());
    }
    if (content.get_id() == td_api::messagePhoto::ID)
    {
        auto&& photos = static_cast<const td_api::messagePhoto &>(content);
        auto&& photo = photos.photo_->sizes_[findLargestPhoto(photos.photo_)];
        getText(photos.caption_);
        getFile(ret, *photo->photo_, photo->height_);
    }
    if (content.get_id() == td_api::messageDocument::ID)
    {
        auto&& document = static_cast<const td_api::messageDocument &>(content);
        getText(document.caption_);
        getFile(ret, *document.document_->document_, 0, document.document_->file_name_);
    }
    if (content.get_id() == td_api::messageSticker::ID)
    {
        auto&& sticker = static_cast<const td_api::messageSticker &>(content).sticker_;
        append(sticker->emoji_);
        getFile(ret, *sticker->thumbnail_->file_, 64);
    }
    if (content.get_id() == td_api::messageAnimatedEmoji::ID)
    {
        auto&& sticker = static_cast<const td_api::messageAnimatedEmoji &>(content).animated_emoji_->sticker_;
        append(sticker->emoji_);
        getFile(ret, *sticker->thumbnail_->file_, 64);
    }
    if (content.get_id() == td_api::messageAnimation::ID)
    {
        auto&& gif = static_cast<const td_api::messageAnimation &>(content).animation_;
        getFile(ret, *gif->animation_, gif->height_);
    }
    if (content.get_id() == td_api::messageVideo::ID)
    {
        auto&& video = static_cast<const td_api::messageVideo &>(content);
        getText(video.caption_);
        getFile(ret, *video.video_->video_, video.video_->height_);
    }
    if (content.get_id() == td_api::messageChatChangeTitle::ID)
    {
        auto&& change = static_cast<const td_api::messageChatChangeTitle &>(content);
        append("<Поменяно название на " + change.title_ + ">");
    }
    if (content.get_id() == td_api::messageLocation::ID)
    {
        auto&& location = static_cast<const td_api::messageLocation &>(content).location_;
        ret.locations.emplace_back(location->longitude_, location->latitude_);
    }
    if (content.get_id() == td_api::messagePinMessage::ID)
    {
        append("<Сообщение закреплено>");
    }
    if (content.get_id() == td_api::messageContactRegistered::ID)
    {
        append("<Контакт присоединился к телеграму!>");
    }
    if (content.get_id() == td_api::messageBasicGroupChatCreate::ID)
    {
        append("<Создан групповой чат>");
    }
    if (content.get_id() == td_api::messageAudio::ID)
    {
        append("<Аудио>");
    }
    if (content.get_id() == td_api::messageContact::ID)
    {
        append("<Контакт>");
    }
    if (content.get_id() == td_api::messageVoiceNote::ID)
    {
        append("<Голосовое сообщение>");
    }
    if (content.get_id() == td_api::messageForumTopicCreated::ID)
    {
        append("<Топик создан>");
    }
    if (content.get_id() == td_api::messageCall::ID)
    {
        auto&& call = static_cast<const td_api::messageCall &>(content);
        append(qs("<Звонок, %1>").arg(getDuration(call.duration_)).toStdString());
    }
    ret.newText = ret.text;
}
// }}}

qs Telegram::getFromId(const td_api::MessageSender& sender)
{
    if (sender.get_id() == td_api::messageSenderUser::ID)
        return qs::number(static_cast<const td_api::messageSenderUser &>(sender).user_id_);
    else
        return qs::number(static_cast<const td_api::messageSenderChat &>(sender).chat_id_);
}

message Telegram::getMessageFromApi(const td_api::message& m)/*{{{*/
{
    qs from_id = getFromId(*m.sender_id_);
    //writeln("Получено сообщение в", qs::number(m.chat_id_), "от", from_id, qs::fromStdString(to_string(m)));
    int date = m.date_;
    ll id = m.id_;
    qs chat_id = qs::number(m.chat_id_);

    bool out = m.is_outgoing_;
    bool isEdited = m.edit_date_ != 0;
    message ret(chat_id, "", id, date, from_id, out, {}, isEdited, false);
    if (m.is_topic_message_)
    {
        ret.topicId = qs::number(m.message_thread_id_);
        //chat_id += ":" + qs::number(m.message_thread_id_);
    }
    auto getReply = [&](ll chat_id, ll mid) {
        auto get = td_api::make_object<td_api::getMessage>(chat_id, mid);
        send_query(std::move(get), errorHandler<td_api::message>("Get reply message", [](td_api::object_ptr<td_api::message> reply) {
            if (!reply) return;
            emit onMessageEdited(getMessageFromApi(*reply));
        }));
    };

    setMessageText(ret, std::move(*m.content_));

    if (m.interaction_info_)
    {
        auto&& interaction = static_cast<const td_api::messageInteractionInfo &>(*m.interaction_info_);
        int erections = 0;
        for (const auto& erection: interaction.reactions_)
            erections += erection->total_count_;
        ret.erections = qs::number(erections);
    }

    if (m.reply_to_)
    {
        if (m.reply_to_->get_id() == td_api::messageReplyToMessage::ID)
        {
            auto&& reply = static_cast<const td_api::messageReplyToMessage &>(*m.reply_to_);
            ret.fwdMessages.push_back(message(chat_id, GREATER "Незагруженное сообщение, на которое ответили" LESS, reply.message_id_));
            getReply(reply.chat_id_, reply.message_id_);
        }
    }
    if (m.forward_info_)
    {
        ll sender;
        if (m.forward_info_->origin_->get_id() == td_api::messageOriginChannel::ID)
            sender = static_cast<const td_api::messageOriginChannel &>(*m.forward_info_->origin_).chat_id_;
        else
            sender = static_cast<const td_api::messageOriginUser &>(*m.forward_info_->origin_).sender_user_id_;
        ret.forwardedFromId = qs::number(sender);
    }
    return ret;
} /*}}}*/

void Telegram::proceedLastDialogs() // {{{
{
    writeln("Proceeding last dialogs...");
    send_query(td_api::make_object<td_api::getChats>(nullptr, 20), errorHandler("Last dialogs"));
    send_query(td_api::make_object<td_api::getMe>(), errorHandler<td_api::user>("Get me", [](td_api::object_ptr<td_api::user> me) {
        emit onUserLoaded(user(me->id_, "Я"));
    }));
    writeln("Proceeded last dialogs");
} //}}}

void Telegram::longPoll() // {{{
{
    while (isRunning)
    {
        if (need_restart) {
            //init();
        } else if (!are_authorized) {
            process_response(client->receive(10));
        } else {
            while (true) {
                auto response = client->receive(0);
                if (response.object) {
                    process_response(std::move(response));
                } else {
                    break;
                }
            }
            this_thread::sleep_for(1s);
        }
    }
} //}}}

void Telegram::startLongPoll() // {{{
{
    writeln("Starting longpoll...");
    if (!isRunning) return writeln("Is not running, stopped");
    delete watcher;
    watcher = new QFutureWatcher<void>();
    connect(watcher, &QFutureWatcher<void>::finished, foo, &Telegram::startLongPoll);
    auto f = QtConcurrent::run([&]() {
        try 
        {
            longPoll();
        }
        catch (const exception& e)
        {
            writeln("Error in longpoll:", e.what());
        }
    });
    watcher->setFuture(f);
    writeln("Started longpoll");
} // }}}

void Telegram::init(const QJsonValue& config) // {{{
{
    writeln("Init is called");
    api_id = getInt(config, "api_id");
    api_hash = getString(config, "api_hash");
    writeln(api_id, api_hash);
    if (api_id == 0 || api_hash == "")
        return writeln("No api_id and/or api_hash in Telegram config");
    Client::execute({0, td_api::make_object<td_api::setLogVerbosityLevel>(1)});
    client = make_unique<Client>();
    startLongPoll();
} //}}}

void Telegram::send_query(td_api::object_ptr<td_api::Function> f, function<void(Object)> handler) // {{{
{
    auto query_id = ++current_query_id;
    if (handler)
        handlers.emplace(query_id, std::move(handler));
    client->send({query_id, std::move(f)});
} // }}}

void Telegram::process_response(Client::Response response) // {{{
{
    if (!response.object)
        return;
    if (response.id == 0)
        return process_update(std::move(response.object));
    auto it = handlers.find(response.id);
    if (it != handlers.end())
        it->second(std::move(response.object));
} // }}}

void Telegram::process_update(Object update) // {{{
{
    downcast_call(
        *update,
        overloaded(
            [](td_api::updateUserStatus&) {}, //Do nothing // {{{
            [](td_api::updateChatLastMessage&) {},
            [](td_api::updateScopeNotificationSettings&) {},
            [](td_api::updateOption&) {},
            [](td_api::updateUserFullInfo&) {},
            [](td_api::updateUnreadMessageCount&) {},
            [](td_api::updateUnreadChatCount&) {},
            [](td_api::updateHavePendingNotifications&) {},
            [](td_api::updateFile&) {},
            [](td_api::updateMessageEdited&) {},
            [](td_api::updateChatNotificationSettings&) {},
            [](td_api::updateBasicGroup&) {},
            [](td_api::updateBasicGroupFullInfo&) {},
            [](td_api::updateSupergroupFullInfo&) {},
            [](td_api::updateSelectedBackground&) {},
            [](td_api::updateDiceEmojis&) {},
            [](td_api::updateAnimationSearchParameters&) {},
            [](td_api::updateMessageUnreadReactions&) {},
            [](td_api::updateChatThemes&) {},
            [](td_api::updateChatAvailableReactions&) {},
            [](td_api::updateChatUnreadReactionCount&) {},
            [](td_api::updateActiveEmojiReactions&) {},
            [](td_api::updateDefaultReactionType&) {},
            [](td_api::updateChatVideoChat&) {},
            [](td_api::updateChatAccentColor&) {},
            [](td_api::updateStoryStealthMode&) {},
            [](td_api::updateChatFolders&) {},
            [](td_api::updateChatBackgroundCustomEmoji&) {},
            [](td_api::updateChatPermissions&) {},
            [](td_api::updateChatHasProtectedContent&) {},
            [](td_api::updateFileDownloads&) {},
            [](td_api::updateAttachmentMenuBots&) {},
            [](td_api::updateAccentColors&) {},
            [](td_api::updateConnectionState&) {}, // }}}
            [](td_api::updateMessageInteractionInfo& interaction) {
                auto get = td_api::make_object<td_api::getMessage>(interaction.chat_id_, interaction.message_id_);
                send_query(std::move(get), errorHandler<td_api::message>("Get interaction message", [](td_api::object_ptr<td_api::message> reply) {
                    if (!reply) return;
                    emit onMessageEdited(getMessageFromApi(*reply));
                }));
            },
            [](td_api::updateDeleteMessages& deleted) {
                if (deleted.is_permanent_)
                    for (auto& id: deleted.message_ids_)
                    {
                        message ret(qs::number(deleted.chat_id_), "", id);
                        ret.isDeleted = true;
                        emit onMessageEdited(ret);
                    }
            },
            [&](td_api::updateAuthorizationState &update_authorization_state) {
                authorization_state = std::move(update_authorization_state.authorization_state_);
                on_authorization_stateupdate();
            },
            [&](td_api::updateChatReadInbox& chat_read) {
                emit onUserUpdated(qs::number(chat_read.chat_id_), chat_read.last_read_inbox_message_id_, -1, chat_read.unread_count_);
            },
            [&](td_api::updateChatReadOutbox& chat_read) {
                emit onUserUpdated(qs::number(chat_read.chat_id_), -1, chat_read.last_read_outbox_message_id_, -1);
            },
            [&](td_api::updateChatAction& chat_action) {
                if (chat_action.action_->get_id() == td_api::chatActionTyping::ID)
                    emit userIsTyping(qs::number(chat_action.chat_id_), getFromId(*chat_action.sender_id_));
            },
            [&](td_api::updateNewChat &update_new_chat) {
                //writeln("New chat", qs::fromStdString(to_string(update_new_chat)));
                auto raw_id = update_new_chat.chat_->id_;
                qs id = qs::number(raw_id);
                qs title = qs::fromStdString(update_new_chat.chat_->title_);
                if (id[0] == '-') title = "Чат: " + title;
                emit onUserLoaded(user(id, title));
                emit onUserUpdated(id, update_new_chat.chat_->last_read_inbox_message_id_, update_new_chat.chat_->last_read_outbox_message_id_, 0);
            },
            [&](td_api::updateForumTopicInfo &ft) { 
                auto chat_id = ft.chat_id_;
                auto tid = ft.info_->message_thread_id_;
                qs id = qs::number(chat_id) + ":" + qs::number(tid);
                qs title = qs::fromStdString(ft.info_->name_);
                writeln("New topic", id, title);
                emit onUserLoaded(user(id, title));
            },
            [&](td_api::updateSupergroup &sg) {
                auto sg_id = sg.supergroup_->id_;
                if (forums_loaded[sg_id]) return;

                auto chat_id = -1000000000000 - sg_id;
                auto get = td_api::make_object<td_api::getForumTopics>();
                get->chat_id_ = chat_id;
                get->limit_ = 100;
                send_query(std::move(get), errorHandler<td_api::forumTopics>("Get forum threads", [sg_id](td_api::object_ptr<td_api::forumTopics> reply) {
                    if (!reply) return;
                    forums_loaded[sg_id] = true;
                }));
            },
            [&](td_api::updateUser &update_user) {
                //writeln("User updated", qs::fromStdString(to_string(update_user)));
                qs id = qs::number(update_user.user_->id_);
                qs title = qs::fromStdString(update_user.user_->first_name_ + " " + update_user.user_->last_name_);
                emit onUserLoaded(user(id, title));
            },
            [&](td_api::updateNewMessage &update_new_message) {
                auto state = std::move(update_new_message.message_->sending_state_);
                if (!state || (state->get_id() != td_api::messageSendingStatePending::ID && state->get_id() != td_api::messageSendingStateFailed::ID))
                    emit onMessageRecieved(getMessageFromApi(*update_new_message.message_));
            },
            [](td_api::updateChatTitle &update_chat_title) {
                qs user_id = qs::number(update_chat_title.chat_id_);
                message ret(user_id, qs::fromStdString("<Чат переименован в " + update_chat_title.title_ + ">"), 0, 0, "", false);
                emit onMessageRecieved(ret);
            },
            [&](td_api::updateMessageSendSucceeded& message) {
                emit onMessageRecieved(getMessageFromApi(*message.message_));
            },
            [&](td_api::updateMessageSendFailed& failed) {
                auto ret = getMessageFromApi(*failed.message_);
                ret.text = join(qs::fromStdString("<Сообщение не отправлено>"), ret.text, qs::fromStdString("<Сообщение не отправлено>"));
                //ret.text = join(qs::fromStdString("<Сообщение не отправлено по причине " + failed.error_message_ + ">"), ret.text);
                emit onMessageRecieved(ret);
            },
            [&](td_api::updateMessageContent& update) {
                //writeln("Update", qs::fromStdString(to_string(update.new_content_)));
                message ret(qs::number(update.chat_id_), "", update.message_id_);
                setMessageText(ret, std::move(*update.new_content_));
                ret.isEdited = true;
                emit onMessageEdited(ret);
            },
            [&](td_api::updateChatDraftMessage& update) {
                //writeln("Update draft message", qs::fromStdString(to_string(update)));
                if (!update.draft_message_)
                    return;
                auto&& inputText = static_cast<const td_api::inputMessageText &>(*update.draft_message_->input_message_text_).text_->text_;
                message ret(qs::number(update.chat_id_), qs::fromStdString(inputText));
                emit onDraftMessage(ret);
            },
            [&](td_api::updateChatActionBar& update) {
                if (update.action_bar_ && update.action_bar_->get_id() == td_api::updateChatActionBar::ID)
                {
                    message ret(qs::number(update.chat_id_), "<Пользователь заблокирован>");
                    emit onMessageRecieved(ret);
                }
            },
            [&](td_api::updateCall& update) {
                writeln("Call", qs::fromStdString(to_string(update)));
                qs action;
                auto call = std::move(update.call_);
                if (auto id = call->state_->get_id(); id == td_api::callStateDiscarded::ID) {
                    action = "завершён";
                    auto&& reason = static_cast<const td_api::callStateDiscarded &>(*call->state_).reason_;
                    if (auto rid = reason->get_id(); rid == td_api::callDiscardReasonDeclined::ID) {
                        action += " (Отклонён)";
                    } else if (rid == td_api::callDiscardReasonDisconnected::ID) {
                        action += " (Отключено)";
                    } else if (rid == td_api::callDiscardReasonEmpty::ID) {
                        action += " (Отвечен)";
                    } else if (rid == td_api::callDiscardReasonMissed::ID) {
                        action += " (Не отвечен)";
                    }
                } else if (id == td_api::callStateError::ID) {
                    action = "завершён с ошибкой";
                } else if (id == td_api::callStateHangingUp::ID) {
                    action = "завершается";
                } else if (id == td_api::callStateReady::ID) {
                    action = "начат";
                }
                bool isOut = call->is_outgoing_;
                qs m = qs(R"(<%1ходящий %3звонок%2>)").arg(isOut ? "Ис" : "В", action == "" ? action : (" " + action), call->is_video_ ? "видео " : "");
                qs user_id = qs::number(call->user_id_);
                message ret(user_id, m, rand(), 0, user_id, isOut);
                emit onMessageRecieved(ret);
            },
            [&](td_api::updateGroupCall&) {
                writeln("Group chat");
            },
            [&](td_api::updateChatPhoto& update) {
                message ret(qs::number(update.chat_id_), "<Обновлена фотография>", rand()); // This message has no id. Just add rand (getfile updates message by id)
                getFile(ret, *update.photo_->big_, 640);
                emit onMessageRecieved(ret);
            },
            [&](td_api::updateChatPosition& update) {
                if (update.position_->list_)
                    emit onUserHidden(qs::number(update.chat_id_), update.position_->list_->get_id() == td_api::chatListArchive::ID);
            },
            [](auto &update) {
                writeln("Unknown (unneeded) update:", qs::fromStdString(to_string(update)));
            }
        )
    );
} // }}}

function<void(Object)> Telegram::create_authentication_query_handler() // {{{
{
    return [id = authentication_query_id](Object object) {
        if (id == authentication_query_id)
            if (object->get_id() == td_api::error::ID) {
                auto error = move_tl_object_as<td_api::error>(object);
                writeln("Error:", qs::fromStdString(to_string(error)));
                on_authorization_stateupdate();
            }
    };
} // }}}

void Telegram::on_authorization_stateupdate() // {{{
{
    authentication_query_id++;
    downcast_call(
        *authorization_state,
        overloaded(
            [&](td_api::authorizationStateReady &) {
                writeln("Got authorizationStateReady");
                are_authorized = true;
                proceedLastDialogs();
            },
            [&](td_api::authorizationStateLoggingOut &) {
                are_authorized = false;
            },
            [&](td_api::authorizationStateClosing &) {
            },
            [&](td_api::authorizationStateClosed &) {
                are_authorized = false;
                need_restart = true;
            },
            [&](td_api::authorizationStateWaitPhoneNumber &) {
                writeln("Enter phone number:");
                string phone_number;
                cin >> phone_number;
                send_query(td_api::make_object<td_api::setAuthenticationPhoneNumber>(phone_number, nullptr), create_authentication_query_handler());
            },
            [&](td_api::authorizationStateWaitEmailAddress &) {
                writeln("Enter email address:");
                string email_address;
                cin >> email_address;
                send_query(td_api::make_object<td_api::setAuthenticationEmailAddress>(email_address), create_authentication_query_handler());
            },
            [&](td_api::authorizationStateWaitEmailCode &) {
                writeln("Enter email authentication code:");
                string code;
                cin >> code;
                send_query(td_api::make_object<td_api::checkAuthenticationEmailCode>(td_api::make_object<td_api::emailAddressAuthenticationCode>(code)), create_authentication_query_handler());
            },
            [&](td_api::authorizationStateWaitCode &) {
                writeln("Enter authentication code:");
                string code;
                cin >> code;
                send_query(td_api::make_object<td_api::checkAuthenticationCode>(code), create_authentication_query_handler());
            },
            [&](td_api::authorizationStateWaitRegistration &) {
                string first_name;
                string last_name;
                writeln("Enter your first name:");;
                cin >> first_name;
                writeln("Enter your last name:");
                cin >> last_name;
                send_query(td_api::make_object<td_api::registerUser>(first_name, last_name), create_authentication_query_handler());
            },
            [&](td_api::authorizationStateWaitPassword &) {
                writeln("Enter authentication password:");
                string password;
                cin >> password;
                send_query(td_api::make_object<td_api::checkAuthenticationPassword>(password), create_authentication_query_handler());
            },
            [&](td_api::authorizationStateWaitOtherDeviceConfirmation &state) {
                writeln("Confirm this login link on another device:", qs::fromStdString(state.link_));
            },
            [&](td_api::authorizationStateWaitTdlibParameters &) {
                auto cachePath = QStandardPaths::writableLocation(QStandardPaths::CacheLocation);
                qs dirname = cachePath + QDir::separator() + "tdlib";
                auto parameters = td_api::make_object<td_api::setTdlibParameters>();
                parameters->database_directory_ = dirname.toStdString();
                parameters->use_message_database_ = true;
                parameters->use_secret_chats_ = true;
                parameters->api_id_ = api_id;
                parameters->api_hash_ = api_hash.toStdString();
                parameters->system_language_code_ = "en";
                parameters->device_model_ = "Desktop";
                parameters->system_version_ = "Arch 5.15.91-4-lts";
                parameters->application_version_ = "2.5";
                parameters->enable_storage_optimizer_ = true;
                send_query(std::move(parameters), create_authentication_query_handler());
            }
        )
    );
} // }}}

Telegram::Telegram(QObject* parent) : // {{{
    QObject(parent)
{
    isRunning = true;
} //}}}

Telegram::~Telegram() // {{{
{
    send_query(td_api::make_object<td_api::close>(), {});
    isRunning = false;
    delete watcher;
} //}}}

const qs Telegram::name = "Tg";

Telegram* Telegram::foo = new Telegram();
int Telegram::lastTS = 0;
int Telegram::lastMessageId = 0;
volatile bool Telegram::isRunning;
unique_ptr<Client> Telegram::client;
QFutureWatcher<void>* Telegram::watcher;
bool Telegram::are_authorized = false;
bool Telegram::need_restart = false;
ull Telegram::current_query_id = 0;
ull Telegram::authentication_query_id = 0;
map<ll, tuple<message, int, string>> Telegram::files;
map<ll, bool> Telegram::forums_loaded;

ll Telegram::api_id;
qs Telegram::api_hash;

td_api::object_ptr<td_api::AuthorizationState> Telegram::authorization_state;
map<ull, function<void(Object)>> Telegram::handlers;
