#ifndef VK_H
#define VK_H

#include "debug.h"
#include "network.h"
#include "includes.h"
#include "message.h"
#include "user.h"
#include "social.h"

class Vk : public QObject, public SocialInterface
{
    Q_OBJECT
public:
    explicit Vk(QObject *parent = nullptr);
    ~Vk();
    static Vk* foo;
    static const qs name;

    void loadHistory  (qs peerId, int count, ll startMessageId = -1, bool force = false);
    void loadUser     (qs peerId);
    void loadUsers    (const std::vector<qs>& peerIds);
    void markAsRead   (qs peerId, const vector<ll>& messageIds);
    void sendMessage  (qs peerId, qs const& text, map<string, QVariant> options);
    void deleteMessage(message const& m);
    void editMessage  (message const& m);
    void setDraft     (message const& m);
    void hideUser     (qs peerId, bool hidden);
    void erect        (qs peerId, ll messageId);

    void init(const QJsonValue& config);
public slots:
    void startLongPoll();
signals:
    void onUserLoaded(user const& u);
    void onUserUpdated(qs peerId, ll inRead, ll outRead, int unread);
    void onMessageRecieved(message const& m);
    void onMessageEdited(message const& m);
    void log(const QVariantList& list);
    void onUnreadDialogs(int dialogs);
    void userIsTyping(qs peerId, qs userId);
    void onDraftMessage(const message& m);
    void onUserHidden(qs peerId, bool hidden);


private:
    static void longPoll();
    static void proceedLongpollUpdate(const QJsonArray& y, bool loadContent = true);
    static void proceedProfiles(const QJsonArray& profiles);
    static void proceedGroups(const QJsonArray& groups);
    static bool proceedConversation(const QJsonValue& conversation);
    static void proceedLastDialogs();
    template<typename... Tail>
    static void writeln(const Tail&... tail);
    static void print(const QVariantList& list);
    
    static message getMessageFromJson(const QJsonValue& mes);
    static void proceedMessageAttachments(message& ret, const QJsonArray& attachments);
    static void proceedMessageAttachment(message& ret, const QJsonValue& attachment);
    
    static void sendCommand(const QStringList& list, const std::function<void(QJsonObject x)>& callback);
    static std::optional<QJsonObject> request(const qs& method, std::map<qs, qs> queryParams, std::optional<std::pair<qs, qs>> photo = {});
    //api
        static QJsonArray getHistory(qs peerId, int count);
        static void editOrSendMessage(qs peerId, const qs& messageText, const map<string, QVariant>& options);

//variables
    static qs key;
    static volatile bool isRunning;
    static QFutureWatcher<void>* watcher;

    static const qs api;
    static const qs version;
    static const qs MY_ID;
    static const int CHAT_OFFSET = 2000000000;
};

#endif // VK_H
