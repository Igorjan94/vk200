#include "debug.h"
#include "user.h"
#include "message.h"

user::user() = default;

user::user(qs id, const qs& name, bool hidden, bool muted) : id(id), name(name), hidden(hidden), muted(muted) {}
user::user(int id, const qs& name, bool hidden, bool muted) : id(qs::number(id)), name(name), hidden(hidden), muted(muted) {}

user::user(QJsonValue account) :
    name(getString(account, "name")),
    hidden(getValue(account, "hidden").toBool()),
    muted(getValue(account, "muted").toBool()),
    type(getString(account, "type"))
{
    if (int id = getInt(account, "id"); id)
        this->id = qs::number(id);
    else
        this->id = getString(account, "id");
    if (type == "") type = "vk";
}

user::operator qs()
{
    return name + " (" + id + ")";
}

user::operator QJsonObject() const
{
    QJsonObject object;
    object.insert("id", id);
    object.insert("name", name);
    object.insert("muted", muted);
    object.insert("hidden", hidden);
    object.insert("type", type);
    return object;
}

bool user::isRead() const
{
    return muted || messages.empty() || unread == 0;
}

void user::addMessage(const message& m)
{
    messages.emplace_back(m);
}
