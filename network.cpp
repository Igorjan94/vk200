#include "network.h"

QJsonObject network::send(const qs& http, const map<qs, qs>& queryParams, optional<pair<qs, qs>> photo)
{
    //writeln(http);
    QUrl params;
    QUrlQuery query;
    QNetworkAccessManager netAccessMan;
    netAccessMan.setTransferTimeout(30000);
    QNetworkReply *reply;
    QUrl url(http);

    if (!photo.has_value())
    {
        //ПАЩИМУ??? Я ТЕБЯ СПРАШИВАЮ ПЕРВЫЙ ПАРАМЕТР НЕ РАБОТАЕТ ИЛИ НЕТ?
        query.addQueryItem("dummy_param", "dummy_value");
        for (auto& [f, s] : queryParams)
            query.addQueryItem(f, s);
        params.setQuery(query);
        QNetworkRequest networkRequest(url);
        networkRequest.setAttribute(QNetworkRequest::RedirectPolicyAttribute, QNetworkRequest::SameOriginRedirectPolicy);
        networkRequest.setHeader(QNetworkRequest::ContentTypeHeader, "application/x-www-form-urlencoded");

        reply = netAccessMan.post(networkRequest, params.toEncoded(QUrl::RemoveFragment));
    }
    else
    {
        auto [type, filename] = photo.value();
        QHttpMultiPart *multiPart = new QHttpMultiPart(QHttpMultiPart::FormDataType);
        QHttpPart imagePart;
        imagePart.setHeader(QNetworkRequest::ContentDispositionHeader, QVariant("form-data; name=\"" + type + "\"; filename=\"" + filename + "\""));
        QFile *file = new QFile(filename);
        if (!file->open(QIODevice::ReadOnly))
        {
            writeln("Файл не открыт ", filename);
            return {};
        }
        imagePart.setBodyDevice(file);

        file->setParent(multiPart);
        multiPart->append(imagePart);
        QNetworkRequest request(url);
        request.setAttribute(QNetworkRequest::RedirectPolicyAttribute, QNetworkRequest::SameOriginRedirectPolicy);
        reply = netAccessMan.post(request, multiPart);
        multiPart->setParent(reply);
    }
    
    QEventLoop loop;

    QObject::connect(reply, &QNetworkReply::finished, &loop, &QEventLoop::quit);
    QObject::connect(reply, &QNetworkReply::errorOccurred, &loop, &QEventLoop::quit);

    loop.exec();
    if (!reply->error())
        return QJsonDocument::fromJson(reply->readAll()).object();
    else
    {
        writeln("Request errored, retrying in 5 secs...", reply->error());
        this_thread::sleep_for(5s);
        return send(http, queryParams, photo);
    }
}

template<typename... Tail> void network::writeln(const Tail&... tail) // {{{
{
    foo->log(qv(tail...));
} // }}}

network* network::foo = new network();
const qs network::name = "Nw";
