#ifndef GUI_H
#define GUI_H

#include "includes.h"
#include "message.h"
#include "user.h"
#include "vk.h"
#include "telegram.h"
#include "config.h"

namespace Ui {
    class Gui;
}

class HoverHelper: public QObject{
    Q_OBJECT
public:
    using QObject::QObject;
Q_SIGNALS:
    void hovered(const qs& s);
public Q_SLOTS:
    void onHovered(const qs& s){ Q_EMIT hovered(s); }
};

class Gui : public QWidget
{
    Q_OBJECT

public:
//methods
    explicit Gui(QWidget *parent = 0);
    ~Gui();
//variables

public slots:
    //QT
    void onReturn();
    void onItemDoubleClicked(QListWidgetItem* item);
    void keyPressEvent(QKeyEvent* event);
    void dropEvent(QDropEvent *event);
    void dragEnterEvent(QDragEnterEvent *event);
    void closeEvent(QCloseEvent *event);
    void scrollToBottom(bool force = false);

public:
    //MY
    template<typename Social> void addUser         (const user& u                                             );
    template<typename Social> void updateUser      (qs peerId, long long inRead, long long outRead, int unread);
    template<typename Social> void messageRecieved (message const& m                                          );
    template<typename Social> void messageEdited   (message const& m                                          );
    template<typename Social> void print           (QVariantList list                                         );
    template<typename Social> void onUserIsTyping  (qs peerId, qs userId                                      );
    template<typename Social> void onDraftMessage  (const message& u                                          );
    template<typename Social> void onUserIsHidden  (qs peerId, bool hidden                                    );
    void setUnreadDialogs();

protected:
    bool eventFilter(QObject* sender, QEvent* event);

private:
    void markAsRead(const user& u);
    void hideUser(user& u, bool hidden);
    void sendMessage(user& u, qs text, const map<string, QVariant>& options = {});
    void saveConfig();
    void print(QVariantList list);
    void runJS(qs const& js, qs const& peerId = "");
    template<typename Social> void assureUserExists(qs peerId);
    template<typename Social> void assureUserExists(const vector<qs>& peerId);
    void setTyping(const user& u);

    void updateTextInBrowser(user& u, int unread = -1);
    void setText(user& u);
    int indexById(qs peerId);
    template<typename... Tail> void writeln(Tail... tail);

    void switchToUser(qs peerId);
    void forceUpdate();

    void appendMessage(qs peerId, message mes);
    void initializeUser(qs peerId);
    void setUnread(int index, int unread);
    bool existsUser(qs id);

    template<typename Social>
    void connectAll(Social* social);

    Ui::Gui *ui;
    QFont bold, unbold;
    std::vector<user> users;
    std::map<qs, qs> nameById;
    int currentUser = 0;
    int previousUser = 0;
    int countOfMessages = 10;
    int unreadDialogs = 0;
    bool showMuted = false;
    bool showHidden = false;
    ofstream logOfstream;

    static const qs name;
    static const qs initHtml;
    static Config& config;
};

#endif // GUI_H
