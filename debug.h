#ifndef DEBUG_H
#define DEBUG_H

#include "includes.h"

template<typename... Tail>
inline qs join(Tail... tail)
{
    QStringList list, ret;
    (list.append(tail), ...);
    for (const qs& s: list)
        if (s != "")
            ret << s;
    return ret.join("\n");
}

inline bool fileExists(const qs& path)
{
    QFileInfo check_file(path);
    return check_file.exists() && check_file.isFile();
}

inline QJsonValue getValue(QJsonValue value, const qs& what) { const auto& x = what.split('.'); for (const qs& s: x) value = value[s]; return value; }
inline QJsonArray getArray(QJsonValue value, const qs& what) { return getValue(value, what).toArray(); }
inline QJsonArray getArrayOptional(optional<QJsonValue> value, const qs& what, QJsonArray defaultValue = {}) {
    if (value.has_value())
        return getValue(value.value(), what).toArray();
    else
        return defaultValue;
}
inline QString getString(QJsonValue value, const qs& what) { return getValue(std::move(value), what).toString(); }
inline long long getInt(QJsonValue value, const qs& what) { return getValue(std::move(value), what).toInt(); }

template<typename T>
inline qs toString(const T& value)
{
    QJsonDocument doc(value);
    return doc.toJson(QJsonDocument::Indented);
}

template<typename T>
inline qs valueOf(T s) { return qs(s); }
inline qs valueOf(int i) { return qs::number(i); }

inline ostream& operator<<(ostream& os, qs s) { return os << s.toStdString(); }
inline ostream& operator<<(ostream& os, const QJsonValue& v) { return os << toString(v.toObject()); }
inline ostream& operator<<(ostream& os, const QJsonObject& v) { return os << toString(v); }

template<typename... Tail>
inline QVariantList qv(Tail... tail)
{
    QVariantList list;
    (list.append(tail),...);
    return list;
}
[[noreturn]] inline void error(qs reason)
{
    cout << "Fatal error occured:\n    " << reason << "\n";
    exit(1);
}

inline auto readFile(const qs& filename, bool failOnError = false)
{
    QFile file(filename);
    if (!file.open(QIODevice::ReadOnly))
    {
        if (failOnError)
            error("File \"" + filename + "\" is not found");
        else
            return vector<qs>();
    }
    QTextStream in(&file);
    vector<qs> strings;

    while (!in.atEnd())
        strings.push_back(in.readLine());
    file.close();
    return strings;
}

inline QJsonValue loadJson(qs filename) {
    QFile jsonFile(filename);
    if (!jsonFile.open(QIODevice::ReadOnly))
        error("File \"" + filename + "\" is not found");
    return QJsonDocument::fromJson(jsonFile.readAll()).object();
}

inline void saveJson(const QJsonDocument& document, const qs& fileName) {
    QFile jsonFile(fileName);
    jsonFile.open(QFile::WriteOnly);
    jsonFile.write(document.toJson());
}

inline qs tableView(const qs& title, const qs& format, const vector<QStringList>& rows)
{
    if (rows.size() == 0u) return qs();
    vector<decltype(rows[0].size())> maxLen(rows[0].size());
    for (const auto& row: rows)
        for (int i = 0; i < row.size(); ++i)
            maxLen[i] = max(maxLen[i], row[i].size());
    qs ans = title;
    for (const auto& row: rows)
    {
        qs temp = format;
        for (int i = 0; i < row.size(); ++i)
            temp = temp.arg(row[i], -maxLen[i]);
        ans = join(ans, temp);
    }
    return ans;
}

inline qs getDuration(int duration) {
    if (duration < 120) return qs::number(duration) + " c";
    vector<int> durations = { 60 * 60, 60, 1 };
    QStringList ans;
    for (int& d: durations)
    {
        int x = duration / d;
        ans.push_back(qs("%1").arg(x, 2, 10, QLatin1Char('0')));
        duration %= d;
    }
    return ans.join(":");
}

#endif //DEBUG_H
