#-------------------------------------------------
#
# Project created by QtCreator 2016-08-31T17:16:38
#
#-------------------------------------------------

QT       += core gui network webenginewidgets

greaterThan(QT_MAJOR_VERSION, 5): QT += widgets concurrent

TARGET = vk200
TEMPLATE = app

CONFIG += precompile_header
PRECOMPILED_HEADER = debug.h message.h user.h network.h

precompile_header:!isEmpty(PRECOMPILED_HEADER) {
    DEFINES += USING_PCH
}

SOURCES += gui.cpp \
        main.cpp \
        message.cpp \
        network.cpp \
        telegram.cpp \
        textedit.cpp \
        user.cpp \
        vk.cpp \

HEADERS += debug.h \
        gui.h \
        includes.h \
        message.h \
        network.h \
        social.h \
        telegram.h \
        textedit.h \
        user.h \
        vk.h \

FORMS    += vk.ui

LIBS     += -ltdjson_static -ltdjson_private -ltdclient -ltdcore -ltdactor -ltddb -ltdsqlite -ltdnet -ltdutils -lstdc++ -lssl -lcrypto -ldl -lz -lm

;QMAKE_CXX = clang++
QMAKE_CXXFLAGS += -std=c++17
